package com.nekodev.paulina.sadowska.nacodokina.credentials;

import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import static com.nekodev.paulina.sadowska.nacodokina.credentials.SharedPrefsCredentialsManager.SIGN_IN_TOKEN_KEY;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 26.09.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class SharedPrefsCredentialsManagerTest {

    private static final String EXPECTED_USER_TOKEN = "someToken12&";

    private Encryption mEncryption;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;


    @Before
    public void setUp() {
        mSharedPreferences = Mockito.mock(SharedPreferences.class);
        mEditor = Mockito.mock(SharedPreferences.Editor.class);
        when(mSharedPreferences.edit()).thenReturn(mEditor);
        mEncryption = new Encryption() {
            @Override
            public String encrypt(String plainText) {
                return plainText;
            }

            @Override
            public String decrypt(String cipherText) {
                return cipherText;
            }
        };
    }

    @Test
    public void saveCredentials_correctAuthToken_savesTokenAndDate() {
        SharedPrefsCredentialsManager manager = new SharedPrefsCredentialsManager(mSharedPreferences, mEncryption);
        manager.saveCredentials(EXPECTED_USER_TOKEN);
        verify(mEditor).putString(eq(SIGN_IN_TOKEN_KEY), eq(EXPECTED_USER_TOKEN));
    }

    @Test
    public void saveCredentials_nullAuthToken_doesNothing() {
        SharedPrefsCredentialsManager manager = new SharedPrefsCredentialsManager(mSharedPreferences, mEncryption);
        manager.saveCredentials(null);
        verify(mEditor, never()).putString(anyString(), anyString());
    }

    @Test
    public void getToken_returnsSavedToken() {
        when(mSharedPreferences.getString(SIGN_IN_TOKEN_KEY, "")).thenReturn(EXPECTED_USER_TOKEN);
        SharedPrefsCredentialsManager manager = new SharedPrefsCredentialsManager(mSharedPreferences, mEncryption);
        assertEquals(EXPECTED_USER_TOKEN, manager.getToken());
    }

    @Test
    public void areCredentialsSaved_tokenNotSaved_returnsFalse() {
        SharedPrefsCredentialsManager manager = new SharedPrefsCredentialsManager(mSharedPreferences, mEncryption);
        assertEquals(false, manager.areCredentialsSaved());
    }

    @Test
    public void areCredentialsSaved_tokenSaved_returnsTrue() {
        when(mSharedPreferences.getString(SIGN_IN_TOKEN_KEY, "")).thenReturn(EXPECTED_USER_TOKEN);
        SharedPrefsCredentialsManager manager = new SharedPrefsCredentialsManager(mSharedPreferences, mEncryption);
        assertEquals(true, manager.areCredentialsSaved());
    }

    @Test
    public void resetCredentials_writesEmptyCredentialsToEditor() {
        SharedPrefsCredentialsManager manager = new SharedPrefsCredentialsManager(mSharedPreferences, mEncryption);
        manager.resetCredentials();
        verify(mEditor).remove(eq(SIGN_IN_TOKEN_KEY));
    }

}