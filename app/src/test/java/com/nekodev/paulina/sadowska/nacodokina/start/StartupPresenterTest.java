package com.nekodev.paulina.sadowska.nacodokina.start;

import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository;
import com.nekodev.paulina.sadowska.nacodokina.location.LocationFetchListener;
import com.nekodev.paulina.sadowska.nacodokina.location.LocationProvider;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.ImmediateSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 26.09.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class StartupPresenterTest {

    private static final double EXPECTED_LAT = 7.77;
    private static final double EXPECTED_LONG = 6.66;

    private StartupContract.View view;
    private CredentialsManager credentialsManager;
    private CinemasRepository repository;
    private LocationProvider locationProvider;
    private BaseSchedulerProvider schedulerProvider = new ImmediateSchedulerProvider();

    @Before
    public void setUp() {
        view = mock(StartupContract.View.class);
        credentialsManager = mock(CredentialsManager.class);
        repository = mock(CinemasRepository.class);
        locationProvider = locationFetchListener -> locationFetchListener.onSuccess(EXPECTED_LAT, EXPECTED_LONG);
    }

    @Test
    public void subscribe_credentialsSaved_showLogOut() {
        when(credentialsManager.areCredentialsSaved()).thenReturn(true);
        StartupPresenter presenter = createPresenter();
        presenter.subscribe();
        verify(view).showLogOut();
    }

    @Test
    public void subscribe_credentialsNotSaved_showLogInPrompt() {
        when(credentialsManager.areCredentialsSaved()).thenReturn(false);
        StartupPresenter presenter = createPresenter();
        presenter.subscribe();
        verify(view).showLoginPrompt();
    }

    @Test
    public void searchByRadius_radiusEmpty_showsError() {
        StartupPresenter presenter = createPresenter();
        presenter.search("", "", true, false);
        verify(view).showError(anyInt());
    }


    @Test
    public void search_incorrectRadius_showsError() {
        StartupPresenter presenter = createPresenter();
        presenter.search("99x", "", true, false);
        verify(view).showError(anyInt());
    }

    @Test
    public void search_correctRadiusAndCoordinates_viewSearchCalled() {
        StartupPresenter presenter = createPresenter();
        given(repository.getCinemasInArea(any())).willReturn(Single.just(createIdList(1L, 2L, 3L)));
        presenter.subscribe();
        presenter.search("4", "", true, false);
        verify(view).startLoading();
        verify(view).showMoviesForCinemas(anyList());
    }

    @Test
    public void search_radiusNotEmpty_locationError_showError() {
        locationProvider = LocationFetchListener::onError;
        given(repository.getCinemasInArea(any())).willReturn(Single.just(createIdList(1L, 2L, 3L)));
        StartupPresenter presenter = createPresenter();
        presenter.subscribe();
        presenter.search("4", "", true, false);
        verify(view).startLoading();
        verify(view).showError(anyInt());
        verify(view).stopLoading();
    }

    @Test
    public void search_radiusNotEmpty_locationNotFetched_retry() {
        StartupPresenter presenter = createPresenter();
        given(repository.getCinemasInArea(any())).willReturn(Single.just(createIdList(1L, 2L, 3L)));
        presenter.search("4", "", true, false);
        verify(view).startLoading();
        verify(view).showMoviesForCinemas(anyList());
        verify(view).stopLoading();
    }

    @Test
    public void search_byCityName_cinemasFetchError_showError() {
        StartupPresenter presenter = createPresenter();
        given(repository.getCinemasInCity(anyString())).willReturn(Single.error(new Throwable()));
        presenter.search("99x", "", false, true);
        verify(view).showError(anyInt());
    }

    @Test
    public void search_byCityName_showMoviesForCinemas() {
        StartupPresenter presenter = createPresenter();
        given(repository.getCinemasInCity(anyString())).willReturn(Single.just(createIdList(2L, 3L)));
        presenter.search("99x", "some City", false, true);
        verify(view).startLoading();
        verify(view).showMoviesForCinemas(anyList());
        verify(view).stopLoading();
    }

    @Test
    public void signIn_openLoginViewCalled() {
        StartupPresenter presenter = createPresenter();
        presenter.signIn();
        verify(view).openLogin();
    }

    @Test
    public void dismissLoginPrompt_hideLoginPromptCalled() {
        StartupPresenter presenter = createPresenter();
        presenter.dismissLoginPrompt();
        verify(view).hideLoginPrompt();
    }

    @Test
    public void signOut_resetCredentialsAndShowSuccess() {
        StartupPresenter presenter = createPresenter();
        presenter.signOut();
        verify(credentialsManager).resetCredentials();
        verify(view).showLogOutSuccess();
        verify(view).showLoginPrompt();
    }

    private StartupPresenter createPresenter() {
        return new StartupPresenter(view, locationProvider, credentialsManager, repository, schedulerProvider);
    }

    @SafeVarargs
    private final <T> List<T> createIdList(T... id) {
        return new ArrayList<>(Arrays.asList(id));
    }
}