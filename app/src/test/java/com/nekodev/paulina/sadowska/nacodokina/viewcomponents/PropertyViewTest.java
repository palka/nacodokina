package com.nekodev.paulina.sadowska.nacodokina.viewcomponents;

import android.app.Activity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class PropertyViewTest {

    private PropertyView propertyView;

    @Before
    public void setUp(){
        ActivityController<Activity> activityController =
                Robolectric.buildActivity(Activity.class);
        propertyView = new PropertyView(activityController.get());
    }

    @Test
    public void setText_labelTextSet() {
        String expectedValueText = "label text to set";
        propertyView.setText(expectedValueText);
        assertEquals(propertyView.mValue.getText(), expectedValueText);
    }

}