package com.nekodev.paulina.sadowska.nacodokina.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class YoutubeUtilTest {
    @Test
    public void getVideoThumbnail_idNotEmpty_returnsThumbnailUrl() {
        String videoId = "12f%sfnhvi";
        String expectedResult = "http://img.youtube.com/vi/12f%sfnhvi/maxresdefault.jpg";
        String result = YoutubeUtil.getVideoThumbnail(videoId);
        assertEquals(expectedResult, result);
    }

    @Test
    public void getVideoThumbnail_idEmpty_returnsEmptyString() {
        assertEquals("", YoutubeUtil.getVideoThumbnail(""));
    }

    @Test
    public void getVideoThumbnail_idNull_returnsEmptyString() {
        assertEquals("", YoutubeUtil.getVideoThumbnail(null));
    }
}