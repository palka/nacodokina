package com.nekodev.paulina.sadowska.nacodokina.utils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Calendar;
import java.util.Date;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Paulina Sadowska on 26.09.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class ApiDateFormatterTest {

    private ApiDateFormatter apiDateFormatter;

    @Before
    public void setUp() {
        apiDateFormatter = new ApiDateFormatter();
    }

    @Test
    public void format_correctDates_returnsFormattedString() {
        assertEquals("16 września 2017", apiDateFormatter.formatDate("2017-09-16T11:29:48.873Z"));
        assertEquals("1 stycznia 1998", apiDateFormatter.formatDate("1998-01-1T11:29:48.873Z"));
        assertEquals("31 grudnia 2018", apiDateFormatter.formatDate("2018-12-31T11:29:48.873Z"));
    }

    @Test
    public void format_dateNull_returnsEmptyString() {
        assertEquals("", apiDateFormatter.formatDate(null));
    }

    @Test
    public void format_dateEmpty_returnsEmptyString() {
        assertEquals("", apiDateFormatter.formatDate(""));
    }

    @Test
    public void format_incorrectDateFormat_returnsEmptyString() {
        assertEquals("", apiDateFormatter.formatDate("08-09-2017"));
    }

    @Test
    public void parse() {
        Date result = apiDateFormatter.parse("2017-09-16T11:29:48.873Z");
        Calendar cal = Calendar.getInstance();
        cal.setTime(result);
        assertEquals(2017, cal.get(Calendar.YEAR));
        assertEquals(8, cal.get(Calendar.MONTH)); //calendar starts counting months from 0
        assertEquals(16, cal.get(Calendar.DAY_OF_MONTH));
        assertEquals(11, cal.get(Calendar.HOUR));
        assertEquals(29, cal.get(Calendar.MINUTE));
    }

}