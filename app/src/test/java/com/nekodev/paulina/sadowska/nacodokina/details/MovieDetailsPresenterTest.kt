package com.nekodev.paulina.sadowska.nacodokina.details

import com.nekodev.paulina.sadowska.nacodokina.api.CinemaWithShowtimesResponse
import com.nekodev.paulina.sadowska.nacodokina.api.MovieDetailsResponse
import com.nekodev.paulina.sadowska.nacodokina.api.TrailerResponse
import com.nekodev.paulina.sadowska.nacodokina.api.data.MovieRating
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.ImmediateSchedulerProvider
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.*
import java.util.Arrays.asList

/**
 * Created by Paulina Sadowska on 17.06.2018.
 */
class MovieDetailsPresenterTest {

    private val view = mock<MovieDetailsContract.View>()
    private val repository = mock<CinemasRepository>()
    private val credentialsManager = mock<CredentialsManager>()
    private val schedulerProvider = ImmediateSchedulerProvider()

    private val movieDetails = mock<MovieDetailsResponse>()
    private val response = mock<CinemaWithShowtimesResponse>()
    private val trailer = mock<TrailerResponse>()
    private val movieRating = mock<MovieRating>()
    private val presenter = MovieDetailsPresenter(view, repository, schedulerProvider, MOVIE_ID, credentialsManager, asList(1, 2, 3))

    companion object {
        private const val RATING = 5
        private const val MOVIE_ID = 22L
    }

    @Before
    fun setUp() {
        given(repository.getMovieDetails(MOVIE_ID))
                .willReturn(Single.just(movieDetails))
        given(repository.getCinemaShows(eq(MOVIE_ID), anyList()))
                .willReturn(Flowable.just(response))
        given(response.showsInDay).willReturn(mapOf("1" to emptyList(), "2" to emptyList()))
        given(repository.getMoviePhotos(MOVIE_ID))
                .willReturn(Single.just(asList("", "")))
        given(repository.getMovieTrailer(MOVIE_ID))
                .willReturn(Single.just(trailer))
        given(repository.getRatings(MOVIE_ID))
                .willReturn(Single.just(asList(movieRating)))
        given(movieRating.rating).willReturn(RATING.toDouble())
    }

    @Test
    fun subscribe_fetchMovieDetailsSuccess_showMovieDetails() {
        presenter.subscribe()
        verify(view).showDetails(movieDetails)
    }

    @Test
    fun subscribe_fetchPhotosSuccess_showPhotos() {
        presenter.subscribe()
        verify(view).showPhotosCarousel(any())
    }

    @Test
    fun subscribe_fetchPhotosError_doNothing() {
        given(repository.getMoviePhotos(MOVIE_ID))
                .willReturn(Single.error(Throwable()))
        presenter.subscribe()
        verify(view, never()).showError()
    }

    @Test
    fun subscribe_fetchRatingsSuccess_showRatings() {
        presenter.subscribe()
        verify(view).showRatings(any())
    }

    @Test
    fun subscribe_fetchRatingsError_doNothing() {
        given(repository.getRatings(MOVIE_ID))
                .willReturn(Single.error(Throwable()))
        presenter.subscribe()
        verify(view, never()).showError()
    }

    @Test
    fun subscribe_fetchShowsSuccess_initializeDaysAndShowCinemaShows() {
        presenter.subscribe()
        verify(view).initializeDaysPicker(anyList())
        verify(view).showCinemaShows(eq(response), anyString())
    }

    @Test
    fun subscribe_fetchedShowsEmpty_showErrorCalled() {
        given(response.showsInDay).willReturn(emptyMap())
        presenter.subscribe()
        verify(view).showError()
    }


    @Test
    fun onRateClicked_loadAndSaveRating() {
        given(credentialsManager.token).willReturn("token")
        given(repository.rate(anyLong(), anyDouble(), anyString())).willReturn(Completable.complete())
        presenter.onRateClicked(2.3f)
        verify(view).startRatingLoading()
        verify(view).ratingSaved()
        verify(view).stopRatingLoading()
    }

    @Test
    fun onRateClicked_ratingSavingError_loadAndShowError() {
        given(credentialsManager.token).willReturn("token")
        given(repository.rate(anyLong(), anyDouble(), anyString())).willReturn(Completable.error(Throwable()))
        presenter.onRateClicked(2.3f)
        verify(view).startRatingLoading()
        verify(view).showError()
        verify(view).stopRatingLoading()
    }

    @Test
    fun onRateClicked_tokenNull_showError() {
        given(credentialsManager.token).willReturn("token")
        given(repository.rate(anyLong(), anyDouble(), anyString())).willReturn(Completable.error(Throwable()))
        presenter.onRateClicked(2.3f)
        verify(view).showError()
    }

    @Test
    fun onRatingChanged_enableRatingButton() {
        presenter.onRatingChanged()
        verify(view).enableRateButton()
    }
}