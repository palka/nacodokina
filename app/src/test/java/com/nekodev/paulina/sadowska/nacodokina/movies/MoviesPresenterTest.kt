package com.nekodev.paulina.sadowska.nacodokina.movies

import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.ImmediateSchedulerProvider
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.*
import org.mockito.BDDMockito.given


/**
 * Created by Paulina Sadowska on 26.09.2017.
 */
class MoviesPresenterTest {

    private val view = mock<MovieListContract.View>()

    private val repository = mock<CinemasRepository>()

    private val cinemasList = (1L..5L).toList()
    private val movies = (1L..5L).map { Movie(it, "", "") }

    private val credentialsManager = mock<CredentialsManager>()

    private val provider: BaseSchedulerProvider = ImmediateSchedulerProvider()

    @Before
    fun setUp() {
        given(credentialsManager.token).willReturn("")
        given(credentialsManager.areCredentialsSaved()).willReturn(true)
    }

    @Test
    fun subscribe_moviesListNotEmpty_showMovies() {
        given(repository.getMovies(anyList(), anyString())).willReturn(Single.just(movies))
        val presenter = createPresenter()
        presenter.subscribe()
        verify(view).showMovies(anyList())
        verify(view).hideLoading()
    }

    @Test
    fun subscribe_fetchMoviesError_showCriticalError() {
        given(repository.getMovies(anyList(), anyString())).willReturn(Single.error(Throwable()))
        val presenter = createPresenter()
        presenter.subscribe()
        verify(view).showCriticalError()
        verify(view).hideLoading()
    }

    @Test
    fun onItemClicked_movieFound_showDetailsCalled() {
        val presenter = createPresenter()
        presenter.onItemClicked(0)
        verify(view).showMovieDetails(anyLong(), anyList())
    }

    @Test
    fun onItemClicked_showMovieDetails() {
        val presenter = createPresenter()
        presenter.onItemClicked(0)
        verify(view).showMovieDetails(anyLong(), anyList())
    }

    private fun createPresenter(): MoviesPresenter {
        return MoviesPresenter(view, repository, provider, cinemasList, credentialsManager)
    }

}