package com.nekodev.paulina.sadowska.nacodokina.api.data;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Paulina Sadowska on 28.09.2017.
 */
public class CinemasShowsTest {

    private CinemaShows cinemasShows;

    @Before
    public void setUp() {
        Cinema cinema = Mockito.mock(Cinema.class);
        cinemasShows = new CinemaShows(cinema);
    }

    @Test
    public void addShow_newShow_oneShowInMap() {
        String type = "type1";
        String date = "12.08.14";
        cinemasShows.addShow(type, date);
        assertEquals(date, cinemasShows.getShows().get(type).get(0));
    }

    @Test
    public void addShow_twoShowsOneType_twoShowsInMap() {
        String type = "type1";
        String date1 = "12.08.14";
        String date2 = "12.08.14";
        cinemasShows.addShow(type, date1);
        cinemasShows.addShow(type, date2);
        assertEquals(date1, cinemasShows.getShows().get(type).get(0));
        assertEquals(date2, cinemasShows.getShows().get(type).get(1));
    }

    @Test
    public void addShow_twoShowsTwoTypes_twoShowsInMap() {
        String type1 = "type1";
        String type2 = "type2";
        String date1 = "12.08.14";
        String date2 = "12.08.14";
        cinemasShows.addShow(type1, date1);
        cinemasShows.addShow(type2, date2);
        assertEquals(date1, cinemasShows.getShows().get(type1).get(0));
        assertEquals(date2, cinemasShows.getShows().get(type2).get(0));
    }
}