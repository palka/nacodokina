package com.nekodev.paulina.sadowska.nacodokina.login;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.api.AuthTokenResponse;
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.ImmediateSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Paulina Sadowska on 26.09.2017.
 */
@RunWith(RobolectricTestRunner.class)
public class LoginPresenterTest {

    private static final String VALID_PASSWORD = "password123";
    private LoginContract.View view;
    private CredentialsManager manager;
    private CinemasRepository repository;
    private BaseSchedulerProvider provider;

    @Before
    public void setUp() {
        view = Mockito.mock(LoginContract.View.class);
        manager = Mockito.mock(CredentialsManager.class);
        repository = Mockito.mock(CinemasRepository.class);
        provider = new ImmediateSchedulerProvider();
    }

    @Test
    public void subscribe_isSignUpFalse_showSignUpPromptAndTitle() {
        LoginPresenter presenter = createPresenter(false);
        presenter.subscribe();
        verify(view).showSignUpPrompt();
        verify(view).setLayoutTitle(R.string.sign_in);
    }

    @Test
    public void subscribe_isSignUpTrue_showRepeatPasswordAndTitle() {
        LoginPresenter presenter = createPresenter(true);
        presenter.subscribe();
        verify(view).showRepeatPassword();
        verify(view).setLayoutTitle(R.string.sign_up);
    }

    @Test
    public void signIn_loginEmpty_showErrorCalled() {
        LoginPresenter presenter = createPresenter(false);
        presenter.signIn(null, VALID_PASSWORD, VALID_PASSWORD);
        verify(view).onLoginFormError(anyInt());
        presenter.signIn("", VALID_PASSWORD, VALID_PASSWORD);
        verify(view, times(2)).onLoginFormError(anyInt());
    }

    @Test
    public void signIn_passwordEmpty_showErrorCalled() {
        LoginPresenter presenter = createPresenter(false);
        presenter.signIn("login", null, VALID_PASSWORD);
        verify(view).onPasswordFormError(anyInt());
        presenter.signIn("login", "", VALID_PASSWORD);
        verify(view, times(2)).onPasswordFormError(anyInt());
    }

    @Test
    public void signIn_repeatPasswordEmpty_isSignUpTrue_showErrorCalled() {
        LoginPresenter presenter = createPresenter(true);
        presenter.signIn("login", VALID_PASSWORD, null);
        verify(view).onPasswordFormError(anyInt());
        presenter.signIn("login", VALID_PASSWORD, "");
        verify(view, times(2)).onPasswordFormError(anyInt());
    }

    @Test
    public void signIn_repeatPasswordNotEqualsPassword_isSignUpTrue_showErrorCalled() {
        LoginPresenter presenter = createPresenter(true);
        presenter.signIn("login", VALID_PASSWORD, "pass2");
        verify(view).onPasswordFormError(anyInt());
    }


    @Test
    public void signIn_loginSucceeded_showSuccessCalled() {
        mockSuccessLoginResponse("token");
        LoginPresenter presenter = createPresenter(false);
        presenter.signIn("login", VALID_PASSWORD, null);
        verify(view).startLoading();
        verify(manager).saveCredentials("token");
        verify(view).logInSuccess();
        verify(view).stopLoading();
    }

    @Test
    public void signIn_loginFailed_showErrorCalled() {
        when(repository.login(anyString(), anyString())).thenReturn(Single.error(new Throwable()));
        LoginPresenter presenter = createPresenter(false);
        presenter.signIn("login", VALID_PASSWORD, "");
        verify(view).startLoading();
        verify(view).onError(anyInt());
        verify(view).stopLoading();
    }

    @Test
    public void signIn_register_passwordLessThan5Chars_showError() {
        mockSuccessRegisterResponse("token");
        LoginPresenter presenter = createPresenter(true);
        presenter.signIn("login", "pas1", "pas1");
        verify(view).onPasswordFormError(anyInt());
    }

    @Test
    public void signIn_registrationSucceeded_showRegisterSuccessCalled() {
        mockSuccessRegisterResponse("token");
        LoginPresenter presenter = createPresenter(true);
        presenter.signIn("login", VALID_PASSWORD, VALID_PASSWORD);
        verify(view).startLoading();
        verify(view).showRegisterSuccessAndFinish();
        verify(view).stopLoading();
    }

    @Test
    public void signIn_registrationError_showErrorCalled() {
        when(repository.register(anyString(), anyString())).thenReturn(Single.error(new Throwable()));
        LoginPresenter presenter = createPresenter(true);
        presenter.signIn("login", VALID_PASSWORD, VALID_PASSWORD);
        verify(view).startLoading();
        verify(view).onError(anyInt());
        verify(view).stopLoading();
    }

    private void mockSuccessLoginResponse(String token) {
        AuthTokenResponse authToken = new AuthTokenResponse(token, 3);
        when(repository.login(anyString(), anyString())).thenReturn(Single.just(authToken));
    }

    private void mockSuccessRegisterResponse(String token) {
        AuthTokenResponse authToken = new AuthTokenResponse(token, 0);
        when(repository.register(anyString(), anyString())).thenReturn(Single.just(authToken));
    }

    private LoginPresenter createPresenter(boolean isSignUp) {
        return new LoginPresenter(view, manager, repository, provider, isSignUp);
    }

}