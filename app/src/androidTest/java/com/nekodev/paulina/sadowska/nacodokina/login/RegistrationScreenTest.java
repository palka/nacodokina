package com.nekodev.paulina.sadowska.nacodokina.login;

import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.nekodev.paulina.sadowska.nacodokina.MockAppComponent;
import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.CORRECT_LOGIN;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.CORRECT_PASSWORD;
import static com.nekodev.paulina.sadowska.nacodokina.matchers.ToastMatcher.assertToastDisplayed;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */
@RunWith(AndroidJUnit4.class)
public class RegistrationScreenTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityRule
            = new ActivityTestRule<>(
            LoginActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False to customize the intent

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        MoviesApplication app
                = (MoviesApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setAppComponent(new MockAppComponent());
        activityRule.launchActivity(LoginActivity.getStartSignUpIntent());
    }
    @Test
    public void signUp_correctAuthData_finishesActivity() {
        onView(withId(R.id.login_login_edit))
                .perform(typeText(CORRECT_LOGIN));
        onView(withId(R.id.login_password_edit))
                .perform(typeText(CORRECT_PASSWORD))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_password_repeat_edit))
                .perform(typeText(CORRECT_PASSWORD))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_sign_in_button)).perform(click());
        assertTrue(activityRule.getActivity().isFinishing());
    }

    @Test
    public void signUp_incorrectLogin_showsError() {
        onView(withId(R.id.login_login_edit))
                .perform(typeText("some login"));
        onView(withId(R.id.login_password_edit))
                .perform(typeText(CORRECT_PASSWORD))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_password_repeat_edit))
                .perform(typeText(CORRECT_PASSWORD))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_sign_in_button)).perform(click());
        assertFalse(activityRule.getActivity().isFinishing());
        assertToastDisplayed(activityRule, R.string.error_occurred);
    }

    @Test
    public void signUp_unequalPasswords_showsError() {
        onView(withId(R.id.login_login_edit))
                .perform(typeText(CORRECT_LOGIN));
        onView(withId(R.id.login_password_edit))
                .perform(typeText(CORRECT_PASSWORD))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_password_repeat_edit))
                .perform(typeText("wrongpassword"))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_sign_in_button)).perform(click());
        assertFalse(activityRule.getActivity().isFinishing());
    }

}
