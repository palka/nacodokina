package com.nekodev.paulina.sadowska.nacodokina.login;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.nekodev.paulina.sadowska.nacodokina.MockAppComponent;
import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.CORRECT_LOGIN;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.CORRECT_PASSWORD;
import static com.nekodev.paulina.sadowska.nacodokina.matchers.ToastMatcher.assertToastDisplayed;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */
@RunWith(AndroidJUnit4.class)
public class LoginScreenTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityRule
            = new ActivityTestRule<>(
            LoginActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False to customize the intent

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        MoviesApplication app
                = (MoviesApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setAppComponent(new MockAppComponent());
        activityRule.launchActivity(new Intent());
    }

    @Test
    public void logIn_correctLoginAndPassword_finishesActivity() {
        onView(withId(R.id.login_login_edit))
                .perform(typeText(CORRECT_LOGIN));
        onView(withId(R.id.login_password_edit))
                .perform(typeText(CORRECT_PASSWORD))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_sign_in_button)).perform(click());
        assertTrue(activityRule.getActivity().isFinishing());
    }

    @Test
    public void logIn_incorrectLogin_showsToast() {
        onView(withId(R.id.login_login_edit))
                .perform(typeText("incorrectLogin"));
        onView(withId(R.id.login_password_edit))
                .perform(typeText(CORRECT_PASSWORD))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_sign_in_button)).perform(click());
        assertToastDisplayed(activityRule, R.string.incorrect_login_error);
    }

    @Test
    public void logIn_incorrectPassword_showsToast() {
        onView(withId(R.id.login_login_edit))
                .perform(typeText(CORRECT_LOGIN));
        onView(withId(R.id.login_password_edit))
                .perform(typeText("some password"))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.login_sign_in_button)).perform(click());
        assertFalse(activityRule.getActivity().isFinishing());
        assertToastDisplayed(activityRule, R.string.incorrect_login_error);
    }


    @Test
    public void signUpClicked_opensSignUpScreen() {
        onView(withId(R.id.login_sign_up_button))
                .perform(click());
        onView(withId(R.id.login_password_repeat_edit))
                .check(matches(isDisplayed()));
        onView(withId(R.id.login_login_edit))
                .check(matches(isDisplayed()));
    }
}