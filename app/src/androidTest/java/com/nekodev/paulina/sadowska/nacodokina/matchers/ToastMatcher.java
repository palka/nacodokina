package com.nekodev.paulina.sadowska.nacodokina.matchers;

import android.support.annotation.StringRes;
import android.support.test.rule.ActivityTestRule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */

public class ToastMatcher {

    public static void assertToastDisplayed(ActivityTestRule activityRule, @StringRes int message) {
        onView(withText(message)).inRoot(withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }
}
