package com.nekodev.paulina.sadowska.nacodokina.details;

import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.RatingBar;

import com.nekodev.paulina.sadowska.nacodokina.MockAppComponent;
import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.matchers.PropertyImageViewTextMatcher;
import com.nekodev.paulina.sadowska.nacodokina.matchers.PropertyViewTextMatcher;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.AGE_LIMIT;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.CREW;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.DESCRIPTION;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.DIRECTOR;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.DURATION;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.GENRE;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.ORIGINAL_TITLE;
import static com.nekodev.paulina.sadowska.nacodokina.MockAppComponent.TITLE;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */
@RunWith(AndroidJUnit4.class)
public class MovieDetailsScreenTest {


    private static final Long MOVIE_ID = 77L;
    @Rule
    public ActivityTestRule<MovieDetailsActivity> activityRule
            = new ActivityTestRule<>(
            MovieDetailsActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False to customize the intent
    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        MoviesApplication app
                = (MoviesApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setAppComponent(new MockAppComponent());
        activityRule.launchActivity(MovieDetailsActivity.getStartIntent(MOVIE_ID, Collections.emptyList()));
    }

    @Test
    public void openActivityWithExtras_movieDetailsDisplayed() {
        assertTextMatches(R.id.textOriginalTitle, ORIGINAL_TITLE);
        assertTextMatches(R.id.textTitle, TITLE);
        assertTextMatches(R.id.textDescription, DESCRIPTION);
        assertPropertyMatches(R.id.propertyCrew, CREW);
        assertPropertyMatches(R.id.propertyGenre, GENRE);
        assertPropertyMatches(R.id.propertyAgeLimit, AGE_LIMIT);
        assertPropertyMatches(R.id.propertyDirector, DIRECTOR);
        assertPropertyImageMatches(R.id.propertyDuration, DURATION);
    }

    private void assertPropertyImageMatches(int layoutId, String textToMatch) {
        Espresso.onView(ViewMatchers.withId((layoutId)))
                .check(ViewAssertions.matches(
                        PropertyImageViewTextMatcher.withValueText(
                                Matchers.containsString(textToMatch))));
    }

    private void assertPropertyMatches(int layoutId, String textToMatch) {
        Espresso.onView(ViewMatchers.withId((layoutId)))
                .check(ViewAssertions.matches(
                        PropertyViewTextMatcher.withValueText(
                                Matchers.containsString(textToMatch))));
    }

    private void assertTextMatches(int layoutId, String textToMatch) {
        onView(withId(layoutId))
                .check(matches(withText(textToMatch)));
    }

    public final class SetRating implements ViewAction {

        @Override
        public Matcher<View> getConstraints() {
            return isAssignableFrom(RatingBar.class);
        }

        @Override
        public String getDescription() {
            return "Custom view action to set rating.";
        }

        @Override
        public void perform(UiController uiController, View view) {
            RatingBar ratingBar = (RatingBar) view;
            ratingBar.setRating(5);
        }
    }
}