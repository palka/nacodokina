package com.nekodev.paulina.sadowska.nacodokina.credentials;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Paulina Sadowska on 24.09.2017.
 */
@RunWith(AndroidJUnit4.class)
public class ConcealEncryptionTest {

    private final static String ORIGINAL_TEXT = "textToencrypt12*1";
    private final static String ENCRYPTED_TEXT = "0102FB986CEBC8F89F57548D967BD5818A61799F9AD22B6B3266A2C7EC511C7DFAB7E459F96454DCDBF331D173FE19";

    private Encryption mEncryption;

    @Before
    public void setUp() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        mEncryption = new ConcealEncryption(appContext);
    }

    @Test
    public void encryptAndDecrypt_returnsOriginalText() {
        String encryptedText = mEncryption.encrypt(ORIGINAL_TEXT);
        assertEquals(ORIGINAL_TEXT, mEncryption.decrypt(encryptedText));
    }
}