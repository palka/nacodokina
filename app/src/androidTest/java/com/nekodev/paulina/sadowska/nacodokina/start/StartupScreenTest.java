package com.nekodev.paulina.sadowska.nacodokina.start;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.nekodev.paulina.sadowska.nacodokina.MockAppComponent;
import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.nekodev.paulina.sadowska.nacodokina.matchers.ToastMatcher.assertToastDisplayed;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */
@RunWith(AndroidJUnit4.class)
public class StartupScreenTest {


    @Rule
    public ActivityTestRule<StartupActivity> activityRule
            = new ActivityTestRule<>(
            StartupActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False to customize the intent

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        MoviesApplication app
                = (MoviesApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setAppComponent(new MockAppComponent());
        activityRule.launchActivity(new Intent());
    }

    @Test
    public void signOutClicked_loggedIn_showsLoginPrompt() {
        onView(withId(R.id.startup_log_out_button))
                .perform(click());
        assertToastDisplayed(activityRule, R.string.log_out_success);
        onView(withId(R.id.startup_login_layout))
                .check(matches(isDisplayed()));
    }

    @Test
    public void signInClicked_loggedOut_opensLoginActivity() {
        onView(withId(R.id.startup_log_out_button))
                .perform(click());
        onView(withId(R.id.startup_login_button))
                .perform(click());
        onView(withId(R.id.login_login_edit))
                .check(matches(isDisplayed()));
    }

    @Test
    public void searchClicked_correctRadius_opensMoviesActivity() {
        onView(withId(R.id.startup_search_button))
                .perform(click());
        onView(withId(R.id.recyclerViewMovieList))
                .check(matches(isDisplayed()));
    }
}