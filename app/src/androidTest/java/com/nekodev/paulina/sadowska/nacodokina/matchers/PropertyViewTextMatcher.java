package com.nekodev.paulina.sadowska.nacodokina.matchers;

import android.support.annotation.NonNull;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.view.View;

import com.nekodev.paulina.sadowska.nacodokina.viewcomponents.PropertyView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */

public final class PropertyViewTextMatcher {

    @NonNull
    public static Matcher<View> withValueText(final Matcher<String> stringMatcher) {

        return new BoundedMatcher<View, PropertyView>(PropertyView.class) {

            @Override
            public void describeTo(Description description) {
                description.appendText("property wist value: ");
                stringMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(PropertyView item) {
                return stringMatcher.matches(item.getValue());
            }
        };
    }
}