package com.nekodev.paulina.sadowska.nacodokina.credentials;

import android.content.SharedPreferences;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

/**
 * Created by Paulina Sadowska on 23.09.2017.
 */

public class SharedPrefsCredentialsManager implements CredentialsManager {

    @VisibleForTesting
    static final String SIGN_IN_TOKEN_KEY = "sign_in_token_key";

    private final SharedPreferences sharedPreferences;
    private final Encryption mEncryption;


    public SharedPrefsCredentialsManager(SharedPreferences sharedPreferences, Encryption encryption) {
        this.sharedPreferences = sharedPreferences;
        mEncryption = encryption;
    }


    @Override
    public void saveCredentials(String token) {
        if (token == null) {
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SIGN_IN_TOKEN_KEY, mEncryption.encrypt(token));
        editor.apply();
    }


    @Override
    public String getToken() {
        String savedPasswordHash = sharedPreferences.getString(SIGN_IN_TOKEN_KEY, "");
        if (TextUtils.isEmpty(savedPasswordHash)) {
            return "";
        }
        return mEncryption.decrypt(savedPasswordHash);
    }

    @Override
    public boolean areCredentialsSaved() {
        return !(TextUtils.isEmpty(getToken()));
    }

    @Override
    public void resetCredentials() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SIGN_IN_TOKEN_KEY);
        editor.apply();
    }
}
