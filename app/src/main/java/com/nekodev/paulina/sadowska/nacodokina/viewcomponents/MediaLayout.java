package com.nekodev.paulina.sadowska.nacodokina.viewcomponents;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * Created by Paulina Sadowska on 14.10.2017.
 */

public class MediaLayout extends FrameLayout {

    @BindView(R.id.media_image)
    ImageView mImage;

    @BindView(R.id.media_play_button)
    ImageView mPlayButton;


    public MediaLayout(@NonNull Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.media, this, true);
        ButterKnife.bind(this, view);
    }

    public MediaLayout setImageUrl(String imageUrl, boolean blur) {
        RequestCreator requestCreator = Picasso.get()
                .load(imageUrl);
        if (blur) {
            requestCreator.transform(new BlurTransformation(getContext(), 2));
        }
        requestCreator.into(mImage);
        return this;
    }

    public MediaLayout setPlayButtonVisibility(boolean isVisible) {
        if (isVisible) {
            mPlayButton.setVisibility(VISIBLE);
        } else {
            mPlayButton.setVisibility(GONE);
        }
        return this;
    }

}
