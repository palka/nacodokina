package com.nekodev.paulina.sadowska.nacodokina.api.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Paulina Sadowska on 28.09.2017.
 */

public class CinemaShows {
    private Cinema cinema;

    private Map<String, List<String>> shows;

    public CinemaShows(Cinema cinema) {
        this.cinema = cinema;
        this.shows = new HashMap<>();
    }

    public void addShow(String type, String time) {
        List<String> times;
        if (!shows.containsKey(type)) {
            times = new ArrayList<>();
        } else {
            times = shows.get(type);
        }
        times.add(time);
        shows.put(type, times);
    }

    public Map<String, List<String>> getShows() {
        return shows;
    }

    public Cinema getCinema() {
        return cinema;
    }
}
