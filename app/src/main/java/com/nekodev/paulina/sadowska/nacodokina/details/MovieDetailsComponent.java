package com.nekodev.paulina.sadowska.nacodokina.details;

import com.nekodev.paulina.sadowska.nacodokina.ApplicationComponent;
import com.nekodev.paulina.sadowska.nacodokina.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 18.09.2017.
 */
@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = MovieDetailsModule.class)
public interface MovieDetailsComponent {
    void inject(MovieDetailsActivity movieDetailsActivity);
}

