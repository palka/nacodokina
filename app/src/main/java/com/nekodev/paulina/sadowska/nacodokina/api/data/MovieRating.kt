package com.nekodev.paulina.sadowska.nacodokina.api.data

/**
 * Created by Paulina Sadowska on 12.05.2018.
 */
data class MovieRating(val name: String, val rating: Double)
