package com.nekodev.paulina.sadowska.nacodokina.movies

import android.support.annotation.StringRes
import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.base.LegacyBasePresenter
import com.nekodev.paulina.sadowska.nacodokina.base.BaseView

/**
 * Created by Paulina Sadowska on 30.04.2018.
 */
internal interface MovieListContract {
    interface View : BaseView<Presenter> {
        fun showMovies(movies: List<Movie>)

        fun showMovieDetails(movieId: Long, cinemaIds: List<Long>)

        fun hideLoading()

        fun onError(@StringRes errorMessage: Int)

        fun showCriticalError()
    }

    interface Presenter : LegacyBasePresenter {
        fun onItemClicked(movieId: Long)
    }
}