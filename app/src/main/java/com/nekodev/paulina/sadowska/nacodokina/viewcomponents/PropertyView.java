package com.nekodev.paulina.sadowska.nacodokina.viewcomponents;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.nacodokina.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 19.09.2017.
 */

public class PropertyView extends LinearLayout {

    @BindView(R.id.property_title)
    TextView mTitle;

    @BindView(R.id.property_value)
    TextView mValue;

    private CharSequence mTitleText;

    public PropertyView(Context context) {
        super(context);
        mTitleText = "";
        initializeViews(context);
    }

    public PropertyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeAttrs(context, attrs);
        initializeViews(context);

    }

    private void initializeAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PropertyView);
        mTitleText = typedArray.getText(R.styleable.PropertyView_item_title);
        typedArray.recycle();
    }


    @SuppressLint("SetTextI18n")
    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.property_layout, this, true);
        ButterKnife.bind(this, view);
        mTitle.setText(mTitleText + ":");
    }

    public void setText(String value) {
        if (value.isEmpty()) {
            mValue.setText(R.string.no_information);
        } else {
            mValue.setText(value);
        }
    }

    @VisibleForTesting
    public String getValue() {
        return mValue.getText().toString();
    }
}
