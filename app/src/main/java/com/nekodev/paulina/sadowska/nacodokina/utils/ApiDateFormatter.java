package com.nekodev.paulina.sadowska.nacodokina.utils;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Paulina Sadowska on 21.09.2017.
 */

public class ApiDateFormatter implements DateFormatter {
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String DISPLAY_DATE_FORMAT = "d MMMM yyyy";
    private static final String DISPLAY_TIME_FORMAT = "HH:mm";

    @Override
    public String formatDate(String date) {
        return format(date, DISPLAY_DATE_FORMAT);
    }

    @Override
    public String formatTime(String date) {
        return format(date, DISPLAY_TIME_FORMAT);
    }

    @Override
    public Date parse(String date) {
        if (TextUtils.isEmpty(date)) {
            return new Date();
        }
        try {
            return new SimpleDateFormat(DATE_FORMAT, Locale.UK).parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String format(String date, String displayFormat) {
        if (TextUtils.isEmpty(date)) {
            return "";
        }
        try {
            Date resultDate = new SimpleDateFormat(DATE_FORMAT, Locale.UK).parse(date);
            return new SimpleDateFormat(displayFormat).format(resultDate);
        } catch (ParseException e) {
            return "";
        }
    }
}
