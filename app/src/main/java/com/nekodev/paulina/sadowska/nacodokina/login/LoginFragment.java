package com.nekodev.paulina.sadowska.nacodokina.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.base.BaseFragment;
import com.nekodev.paulina.sadowska.nacodokina.rate.RateActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

public class LoginFragment extends BaseFragment<LoginContract.Presenter> implements LoginContract.View {

    @BindView(R.id.login_layout_title)
    TextView mTitle;

    @BindView(R.id.login_login_edit)
    EditText mLoginEdit;

    @BindView(R.id.login_password_edit)
    EditText mPasswordEdit;

    @BindView(R.id.login_password_repeat_edit)
    EditText mPasswordRepeatEdit;

    @BindView(R.id.login_progress)
    ProgressBar mLoading;

    @BindView(R.id.login_sign_in_button)
    TextView mSignInButton;

    @BindView(R.id.login_sign_up_prompt)
    RelativeLayout mSignUpPrompt;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void logInSuccess() {
        getActivity().setResult(LoginActivity.RESULT_OK);
        getActivity().finish();
    }

    @Override
    public void onError(int errorMessage) {
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginFormError(int errorMassage) {
        mLoginEdit.requestFocus();
        mLoginEdit.setError(getString(errorMassage));

    }

    @Override
    public void onPasswordFormError(int errorMassage) {
        mPasswordEdit.requestFocus();
        mPasswordEdit.setError(getString(errorMassage));
    }

    @Override
    public void clearFormErrors() {
        mLoginEdit.setError(null);
        mPasswordEdit.setError(null);
    }

    @Override
    public void startLoading() {
        mLoading.setVisibility(View.VISIBLE);
        mSignInButton.setVisibility(View.GONE);
    }

    @Override
    public void stopLoading() {
        mLoading.setVisibility(View.GONE);
        mSignInButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRepeatPassword() {
        mPasswordRepeatEdit.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSignUpPrompt() {
        mSignUpPrompt.setVisibility(View.VISIBLE);
    }

    @Override
    public void setLayoutTitle(int title) {
        mTitle.setText(title);
        mSignInButton.setText(title);
    }

    @Override
    public void showRegisterSuccessAndFinish() {
        Toast.makeText(getContext(), R.string.register_success, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void showRateActivity() {
        RateActivity.Companion.startActivity(getContext());
    }

    @OnClick(R.id.login_sign_in_button)
    void signInClicked() {
        presenter.signIn(mLoginEdit.getText().toString(),
                mPasswordEdit.getText().toString(),
                mPasswordRepeatEdit.getText().toString());
    }

    @OnClick(R.id.login_sign_up_button)
    public void openSignUp() {
        startActivity(LoginActivity.getStartSignUpIntent(getContext()));
    }
}

