package com.nekodev.paulina.sadowska.nacodokina.base;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

public interface BaseView<T extends LegacyBasePresenter> {
    void setPresenter(T presenter);
}