package com.nekodev.paulina.sadowska.nacodokina.utils

import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast

/**
 * Created by Paulina Sadowska on 26.05.2018.
 */
fun Context.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun Context.showToast(@StringRes messageRes: Int, length: Int = Toast.LENGTH_SHORT) {
    showToast(getString(messageRes))
}