package com.nekodev.paulina.sadowska.nacodokina.login;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

@Module
class LoginModule {

    private final LoginContract.View mView;
    private final boolean isSignUp;

    LoginModule(LoginContract.View view, boolean isSignUp) {
        this.mView = view;
        this.isSignUp = isSignUp;
    }

    @Provides
    LoginContract.View providesView() {
        return mView;
    }

    @Provides
    boolean providesIsSignUp() {
        return isSignUp;
    }
}