package com.nekodev.paulina.sadowska.nacodokina.details;

import android.support.annotation.NonNull;

import com.nekodev.paulina.sadowska.nacodokina.api.CinemaWithShowtimesResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.TrailerResponse;
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.internal.functions.Functions;

/**
 * Created by Paulina Sadowska on 18.09.2017.
 */

public class MovieDetailsPresenter implements MovieDetailsContract.Presenter {

    private final MovieDetailsContract.View view;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable disposable;
    private final CinemasRepository repository;
    private final Long movieId;
    private final CredentialsManager credentialsManager;
    private final List<Long> cinemaIds;


    @Inject
    MovieDetailsPresenter(@NonNull MovieDetailsContract.View view,
                          @NotNull CinemasRepository repository,
                          @NonNull BaseSchedulerProvider schedulerProvider,
                          @NotNull Long movieId,
                          @NotNull CredentialsManager credentialsManager,
                          @NotNull List<Long> cinemaIds) {
        this.view = view;
        this.schedulerProvider = schedulerProvider;
        this.repository = repository;
        this.movieId = movieId;
        this.credentialsManager = credentialsManager;
        this.disposable = new CompositeDisposable();
        this.cinemaIds = cinemaIds;
    }

    @Inject
    void setupListeners() {
        view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        initializeRatingBar();
        getMovieDetails();
        getPhotos();
        getRatings();
        getShows(movieId);
    }

    private void initializeRatingBar() {
        if (credentialsManager.areCredentialsSaved()) {
            disposable.add(repository.getUserRating(movieId, credentialsManager.getToken())
                    .subscribeOn(schedulerProvider.computation())
                    .observeOn(schedulerProvider.ui())
                    .doFinally(view::showRatingBar)
                    .subscribe(view::initializeRatingBar, Functions.emptyConsumer()));

        }

    }

    private void getRatings() {
        disposable.add(repository.getRatings(movieId)
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(view::showRatings, Functions.emptyConsumer()));
    }

    private void getShows(Long movieId) {
        disposable.add(repository.getCinemaShows(movieId, cinemaIds)
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(shows -> {
                    List<String> dates = getDates(shows);
                    if (!dates.isEmpty()) {
                        view.initializeDaysPicker(dates);
                        view.showCinemaShows(shows, dates.get(0));
                    } else {
                        view.showError();
                    }
                }, e -> view.showError()));
    }

    private List<String> getDates(CinemaWithShowtimesResponse cinemaWithShowtimesResponse) {
        Set<String> days = cinemaWithShowtimesResponse.getShowsInDay().keySet();
        return new ArrayList<>(days);
    }

    private void getMovieDetails() {
        disposable.add(repository.getMovieDetails(movieId)
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(view::showDetails, e -> view.showError()));
    }

    private void getPhotos() {
        List<MediaResource> results = new ArrayList<>();
        disposable.add(repository.getMovieTrailer(movieId)
                .map(this::videoToMediaResources)
                .toFlowable()
                .onErrorResumeNext(Flowable.empty())
                .concatWith(repository.getMoviePhotos(movieId)
                        .map(this::imagesToMediaResources)
                        .toFlowable())
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(results::addAll,
                        Functions.emptyConsumer(),
                        () -> view.showPhotosCarousel(results)));
    }

    private List<MediaResource> videoToMediaResources(TrailerResponse video) {
        List<MediaResource> videos = new ArrayList<>();
        videos.add(new VideoResource(video.getThumbnail(), video.getYoutubeId()));
        return videos;
    }

    private List<MediaResource> imagesToMediaResources(List<String> images) {
        List<MediaResource> mediaResources = new ArrayList<>();
        for (String image : images) {
            mediaResources.add(new PhotoResource(image));
        }
        return mediaResources;
    }

    @Override
    public void onRateClicked(float rating) {
        view.startRatingLoading();
        String authToken = credentialsManager.getToken();
        if (authToken != null) {
            rateMovie(authToken, rating);
        } else {
            view.showError();
        }
    }

    private void rateMovie(@NonNull String authToken, double rating) {
        disposable.add(repository.rate(movieId, rating, authToken)
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        () -> {
                            view.ratingSaved();
                            view.stopRatingLoading();
                        },
                        error -> {
                            view.showError();
                            view.stopRatingLoading();
                        }
                ));
    }

    @Override
    public void onRatingChanged() {
        view.enableRateButton();
    }

    @Override
    public void onMediaClicked(MediaResource mediaResource) {
        if (mediaResource instanceof VideoResource) {
            view.playVideo(((VideoResource) mediaResource).getYoutubeId());
        }
    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }
}
