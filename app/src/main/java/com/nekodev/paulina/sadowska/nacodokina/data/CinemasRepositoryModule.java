package com.nekodev.paulina.sadowska.nacodokina.data;

import com.nekodev.paulina.sadowska.nacodokina.api.CinemasService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

@Module
public class CinemasRepositoryModule {

    private static final String HOST = "https://cinemas-shows-api.herokuapp.com/";

    @Provides
    @Singleton
    CinemasService provideCinemasService(Retrofit retrofit) {
        return retrofit.create(CinemasService.class);
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient providerOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }
}