package com.nekodev.paulina.sadowska.nacodokina.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;

import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.utils.ActivityUtils;

import javax.inject.Inject;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String SIGN_UP_KEY = "signUp";

    @Inject
    LoginPresenter mPresenter;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    public static Intent getStartSignUpIntent(Context context) {
        return new Intent(context, LoginActivity.class)
                .putExtra(SIGN_UP_KEY, true);
    }

    @VisibleForTesting
    public static Intent getStartSignUpIntent() {
        return new Intent().putExtra(SIGN_UP_KEY, true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);

        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (loginFragment == null) {
            loginFragment = LoginFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    loginFragment, R.id.contentFrame);
        }
        DaggerLoginComponent.builder()
                .loginModule(new LoginModule(loginFragment, getIntent().getBooleanExtra(SIGN_UP_KEY, false)))
                .applicationComponent(((MoviesApplication) getApplication()).getAppComponent())
                .build()
                .inject(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

