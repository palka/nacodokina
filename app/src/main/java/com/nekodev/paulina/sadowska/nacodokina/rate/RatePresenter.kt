package com.nekodev.paulina.sadowska.nacodokina.rate

import android.os.Bundle
import com.google.android.gms.common.util.CollectionUtils
import com.nekodev.paulina.sadowska.nacodokina.R
import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

/**
 * Created by Paulina Sadowska on 24.05.2018.
 */
class RatePresenter @Inject constructor(
        private val schedulerProvider: BaseSchedulerProvider,
        private val repository: CinemasRepository,
        private val credentialsManager: CredentialsManager)
    : RateContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private var fetchedMovies: List<Movie>? = null
    private var currentMovie = 0
    private var ratedMoviesCount = 0

    private var view: RateContract.View? = null

    companion object {
        const val NUM_MOVIES_USER_SHOULD_RATE = 10
        const val KEY_FETCHED_MOVIES = "fetchedMovies"
        const val KEY_CURRENT_MOVIE = "fetchedMovies"
        const val KEY_RATED_MOVIES_COUNT = "fetchedMovies"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            fetchedMovies = savedInstanceState.getParcelableArrayList(KEY_FETCHED_MOVIES)
            currentMovie = savedInstanceState.getInt(KEY_CURRENT_MOVIE)
            ratedMoviesCount = savedInstanceState.getInt(KEY_RATED_MOVIES_COUNT)
        }
    }

    override fun subscribe(view: RateContract.View) {
        this.view = view
        if (CollectionUtils.isEmpty(fetchedMovies)) {
            fetchMoviesToRate()
        } else {
            showCurrentMovie()
        }
    }

    private fun fetchMoviesToRate() {
        view?.showLoader()
        compositeDisposable.add(repository.fetchPopularMoviesToRate()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeBy(
                        onSuccess = {
                            view?.hideLoader()
                            onMoviesFetchSuccess(it)
                        },
                        onError = {
                            view?.finishActivity()
                        }
                ))
    }

    override fun onRatingChanged(rating: Float) {
        view?.enableRateButton(rating > 0)
    }

    private fun onMoviesFetchSuccess(fetchedMovies: List<Movie>) {
        this.fetchedMovies = fetchedMovies
        currentMovie = 0
        showCurrentMovie()
    }

    override fun onRateClicked(rating: Int) {
        view?.enableRateButton(false)
        view?.showLoader()
        fetchedMovies?.let {
            val movie = it[currentMovie]
            compositeDisposable.add(repository.rate(movie.id, rating.toDouble(), credentialsManager.token)
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .subscribeBy(
                            onComplete = {
                                view?.hideLoader()
                                onRateSuccess()
                            },
                            onError = {
                                view?.hideLoader()
                                view?.showMessage(R.string.error_occurred)
                                view?.enableRateButton(true)
                            }
                    ))
        }

    }

    private fun onRateSuccess() {
        ratedMoviesCount++
        view?.resetRatingBar()
        if (ratedMoviesCount >= NUM_MOVIES_USER_SHOULD_RATE) {
            view?.showMessage(R.string.you_rated_enough_movies)
            stopRating()
        } else {
            moveToNextMovieIfAny()
        }
    }

    override fun onNotSeenClicked() {
        view?.resetRatingBar()
        moveToNextMovieIfAny()
    }

    private fun moveToNextMovieIfAny() {
        currentMovie++
        if (fetchedMovies?.size ?: 0 > currentMovie) {
            showCurrentMovie()
        } else {
            stopRating()
        }
    }

    private fun stopRating() {
        view?.finishActivity()
    }

    private fun showCurrentMovie() {
        fetchedMovies?.get(currentMovie)?.let {
            view?.showMovie(it)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (!CollectionUtils.isEmpty(fetchedMovies)) {
            outState.putParcelableArrayList(KEY_FETCHED_MOVIES, ArrayList<Movie>(fetchedMovies))
            outState.putInt(KEY_CURRENT_MOVIE, currentMovie)
            outState.putInt(KEY_RATED_MOVIES_COUNT, ratedMoviesCount)
        }
    }

    override fun unSubscribe() {
        this.view = null
    }
}