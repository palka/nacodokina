package com.nekodev.paulina.sadowska.nacodokina.movies;

import com.nekodev.paulina.sadowska.nacodokina.ApplicationComponent;
import com.nekodev.paulina.sadowska.nacodokina.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = MoviesModule.class)
public interface MoviesComponent {
    void inject(MoviesActivity moviesActivity);
}
