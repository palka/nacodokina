package com.nekodev.paulina.sadowska.nacodokina.base

import android.os.Bundle

/**
 * Created by Paulina Sadowska on 26.05.2018.
 */
interface BasePresenter<in T> {
    fun onCreate(savedInstanceState: Bundle?)

    fun subscribe(view: T)

    fun onSaveInstanceState(outState: Bundle)

    fun unSubscribe()
}