package com.nekodev.paulina.sadowska.nacodokina.base;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */
@Deprecated
public interface LegacyBasePresenter {
    void subscribe();

    void unSubscribe();
}
