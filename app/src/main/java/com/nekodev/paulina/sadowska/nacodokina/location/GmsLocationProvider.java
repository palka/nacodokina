package com.nekodev.paulina.sadowska.nacodokina.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Paulina Sadowska on 21.09.2017.
 */

public class GmsLocationProvider implements LocationProvider {

    private final Activity mActivity;
    private final FusedLocationProviderClient mFusedLocationClient;
    private final LocationRequest mLocationRequest;

    private LocationFetchListener mListener;

    public GmsLocationProvider(Activity activity) {
        this.mActivity = activity;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mActivity);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void fetchLocation(LocationFetchListener locationFetchListener) {
        mListener = locationFetchListener;
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fetchActualLocation();
            return;
        }
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mListener.onError();
            return;
        }
        getLastLocation();
    }

    @SuppressLint("MissingPermission")
    private void fetchActualLocation() {
        LocationCallback mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                if (location != null) {
                    mListener.onSuccess(location.getLatitude(), location.getLongitude());
                }
                else{
                    mListener.onError();
                }
                mFusedLocationClient.removeLocationUpdates(this);
            }
        };

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */)
                .addOnFailureListener(e -> {
                    mListener.onError();
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                });
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(mActivity, location -> {
                    if (location != null) {
                        mListener.onSuccess(location.getLatitude(), location.getLongitude());
                        return;
                    }
                    mListener.onError();
                });
    }

}
