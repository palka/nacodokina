package com.nekodev.paulina.sadowska.nacodokina.credentials;

/**
 * Created by Paulina Sadowska on 23.09.2017.
 */

public interface Encryption {
    String encrypt(String plainText);

    String decrypt(String cipherText);
}
