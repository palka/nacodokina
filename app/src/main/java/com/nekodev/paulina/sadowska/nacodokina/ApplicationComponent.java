package com.nekodev.paulina.sadowska.nacodokina;

import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepositoryModule;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

@Singleton
@Component(modules = {CinemasRepositoryModule.class, ApplicationModule.class})
public interface ApplicationComponent {

    CredentialsManager getCredentialsManager();

    CinemasRepository getCinemasRepository();

    BaseSchedulerProvider getSchedulerProvider();
}