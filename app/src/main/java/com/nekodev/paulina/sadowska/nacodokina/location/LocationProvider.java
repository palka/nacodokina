package com.nekodev.paulina.sadowska.nacodokina.location;

/**
 * Created by Paulina Sadowska on 21.09.2017.
 */

public interface LocationProvider {
    void fetchLocation(LocationFetchListener locationFetchListener);
}
