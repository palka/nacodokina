package com.nekodev.paulina.sadowska.nacodokina.rate

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication
import com.nekodev.paulina.sadowska.nacodokina.R
import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.utils.hide
import com.nekodev.paulina.sadowska.nacodokina.utils.invisible
import com.nekodev.paulina.sadowska.nacodokina.utils.show
import com.nekodev.paulina.sadowska.nacodokina.utils.showToast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_rate_movie.*
import me.zhanghai.android.materialratingbar.MaterialRatingBar
import javax.inject.Inject

/**
 * Created by Paulina Sadowska on 24.05.2018.
 */
class RateActivity : AppCompatActivity(), RateContract.View {

    @Inject
    lateinit var presenter: RatePresenter

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, RateActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_movie)

        DaggerRateComponent.builder()
                .applicationComponent((application as MoviesApplication).appComponent)
                .build()
                .inject(this)

        presenter.onCreate(savedInstanceState)
        presenter.subscribe(this)

        rateButton.setOnClickListener {
            presenter.onRateClicked(ratingBar.progress)
        }

        notSeenButton.setOnClickListener {
            presenter.onNotSeenClicked()
        }

        ratingBar.onRatingChangeListener = MaterialRatingBar.OnRatingChangeListener { _: MaterialRatingBar, rating: Float ->
            presenter.onRatingChanged(rating)
        }
    }

    override fun enableRateButton(enable: Boolean) {
        rateButton.isEnabled = enable
    }

    override fun resetRatingBar() {
        ratingBar.rating = 0.0f
    }

    override fun showLoader() {
        progressBar.show()
        ratingBar.invisible()
        rateButton.invisible()
        notSeenButton.invisible()
    }

    override fun hideLoader() {
        progressBar.hide()
        ratingBar.show()
        rateButton.show()
        notSeenButton.show()
    }

    override fun showMovie(currentMovie: Movie) {
        cardView.show()
        movieTitle.show()
        movieTitle.text = currentMovie.title
        Picasso.get()
                .load(currentMovie.posterUrl)
                .into(imageMoviePoster)
    }

    override fun showMessage(messageRes: Int) {
        showToast(messageRes)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun finishActivity() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unSubscribe()
    }
}