package com.nekodev.paulina.sadowska.nacodokina.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Paulina Sadowska on 18.09.2017.
 */

public class YoutubeUtil {

    private static final String KEY = "AIzaSyCm1jC7OUju3-haQf8KP4GRfJv8lHzq0EA";
    private static final String VIDEO_THUMBNAIL_BASE = "http://img.youtube.com/vi/%s/maxresdefault.jpg";

    public static void play(Context context, String youtubeId) {
        if (context == null || TextUtils.isEmpty(youtubeId)) {
            return;
        }
        if (YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(context).equals(YouTubeInitializationResult.SUCCESS)) {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) context,
                    KEY,
                    youtubeId,
                    100,     //after this time, video will start automatically
                    true,               //autoplay or not
                    false); //lightbox mode or not; show the video in a small box
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            context.startActivity(intent);
        }
    }

    public static String getVideoThumbnail(String videoId) {
        if (TextUtils.isEmpty(videoId)) {
            return "";
        }
        return String.format(VIDEO_THUMBNAIL_BASE, videoId);
    }

    public static String getVideoId(String videoUrl) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(videoUrl);

        if (matcher.find()) {
            return matcher.group();
        }
        return "";
    }
}
