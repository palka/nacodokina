package com.nekodev.paulina.sadowska.nacodokina.login;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private static final int MIN_PASSWORD_LENGTH = 5;

    private final LoginContract.View view;
    private final CredentialsManager credentialsManager;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable compositeDisposable;
    private final CinemasRepository cinemasRepository;
    private final boolean isSignUp;

    @Inject
    LoginPresenter(@NotNull LoginContract.View mView,
                   @NotNull CredentialsManager credentialsManager,
                   @NotNull CinemasRepository repository,
                   @NonNull BaseSchedulerProvider schedulerProvider,
                   boolean isSignUp) {
        this.view = mView;
        this.credentialsManager = credentialsManager;
        this.schedulerProvider = schedulerProvider;
        this.cinemasRepository = repository;
        this.compositeDisposable = new CompositeDisposable();
        this.isSignUp = isSignUp;
    }

    @Inject
    void setupListeners() {
        view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        if (isSignUp) {
            view.showRepeatPassword();
            view.setLayoutTitle(R.string.sign_up);
        } else {
            view.setLayoutTitle(R.string.sign_in);
            view.showSignUpPrompt();
        }
    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void signIn(String login, String password, String passwordRepeat) {
        view.clearFormErrors();
        if (TextUtils.isEmpty(login)) {
            view.onLoginFormError(R.string.login_empty_error);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            view.onPasswordFormError(R.string.password_empty_error);
            return;
        }
        if (password.length() < MIN_PASSWORD_LENGTH) {
            view.onPasswordFormError(R.string.password_to_short);
            return;
        }

        if (isSignUp) {
            signUp(login, password, passwordRepeat);
        } else {
            signIn(login, password);
        }
    }

    private void signUp(String login, String password, String passwordRepeat) {
        if (TextUtils.isEmpty(passwordRepeat)) {
            view.onPasswordFormError(R.string.password_empty_error);
            return;
        }
        if (!password.equals(passwordRepeat)) {
            view.onPasswordFormError(R.string.passwords_not_same_error);
            return;
        }
        view.startLoading();
        compositeDisposable.add(cinemasRepository.register(login, password)
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        object -> {
                            view.showRegisterSuccessAndFinish();
                            view.stopLoading();
                        },
                        error -> {
                            view.onError(R.string.error_occurred);
                            view.stopLoading();
                        }
                )
        );
    }

    private void signIn(String login, String password) {
        view.startLoading();
        compositeDisposable.add(cinemasRepository.login(login, password)
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        token -> {
                            if (token.getRatedMoviesCount() < 6) {
                                view.showRateActivity();
                            }
                            credentialsManager.saveCredentials(token.getToken());
                            view.logInSuccess();
                            view.stopLoading();
                        },
                        error -> {
                            view.onError(R.string.incorrect_login_error);
                            view.stopLoading();
                        }
                )
        );
    }
}
