package com.nekodev.paulina.sadowska.nacodokina.details

/**
 * Created by Paulina Sadowska on 12.05.2018.
 */
interface MediaResource {
    fun getImageUrl(): String
    fun isPlayButtonVisible(): Boolean
}

class PhotoResource(private val imageUrl: String) : MediaResource {
    override fun getImageUrl(): String {
        return imageUrl
    }

    override fun isPlayButtonVisible() = false


}

class VideoResource(private val thumbnailUrl: String,
                    val youtubeId: String) : MediaResource {
    override fun getImageUrl(): String {
        return thumbnailUrl
    }

    override fun isPlayButtonVisible() = true
}