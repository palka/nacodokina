package com.nekodev.paulina.sadowska.nacodokina.credentials;

import android.content.Context;

import com.facebook.android.crypto.keychain.AndroidConceal;
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.CryptoConfig;
import com.facebook.crypto.Entity;
import com.facebook.crypto.keychain.KeyChain;

/**
 * Created by Paulina Sadowska on 23.09.2017.
 */

public class ConcealEncryption implements Encryption {

    private final Context mContext;

    public ConcealEncryption(Context context){
        this.mContext = context;
    }

    public String encrypt(String plainText) {
        if (mContext == null) {
            return "";
        }

        Crypto crypto = createCrypto(mContext);
        if (!crypto.isAvailable()) {
            return "";
        }
        try {
            byte[] cipherText = crypto.encrypt(hexStringToByteArray(asciiToHex(plainText)), Entity.create("credentials"));
            return byteArrayToHexString(cipherText);
        } catch (Exception e) {
            return "";
        }
    }

    public String decrypt(String cipherText) {
        if (mContext == null) {
            return "";
        }

        Crypto crypto = createCrypto(mContext);

        if (!crypto.isAvailable()) {
            return "";
        }
        try {
            byte[] plainText = crypto.decrypt(hexStringToByteArray(cipherText), Entity.create("credentials"));
            return hexToASCII(byteArrayToHexString(plainText));
        } catch (Exception e) {
            return "";
        }
    }

    private Crypto createCrypto(Context context) {
        KeyChain keyChain = new SharedPrefsBackedKeyChain(context, CryptoConfig.KEY_256);
        return AndroidConceal.get().createDefaultCrypto(keyChain);
    }

    private String asciiToHex(String asciiValue) {
        char[] chars = asciiValue.toCharArray();
        StringBuilder hex = new StringBuilder();
        for (char aChar : chars) {
            hex.append(Integer.toHexString((int) aChar));
        }
        return hex.toString();
    }

    private String hexToASCII(String hexValue) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hexValue.length(); i += 2) {
            String str = hexValue.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        return output.toString();
    }

    private String byteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars);
    }

    private byte[] hexStringToByteArray(String s) throws IllegalArgumentException {
        int len = s.length();
        if (len % 2 == 1) {
            throw new IllegalArgumentException("Hex string must have even number of characters");
        }
        byte[] data = new byte[len / 2]; // Allocate 1 byte per 2 hex characters
        for (int i = 0; i < len; i += 2) {
            // Convert each character into a integer (base-16), then bit-shift into place
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

}

