package com.nekodev.paulina.sadowska.nacodokina.api.data;

import android.support.annotation.VisibleForTesting;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

public class Cinema {

    @SerializedName("id")
    private Long cinemaId;

    @SerializedName("name")
    private String name;

    @SerializedName("address")
    private String address;

    @SerializedName("cinemaUrl")
    private String website;

    @SerializedName("networkName")
    private String networkName;


    public Cinema setName(String name) {
        this.name = name;
        return this;
    }

    public Cinema setAddress(String address) {
        this.address = address;
        return this;
    }

    public Cinema setNetworkName(String networkName) {
        this.networkName = networkName;
        return this;
    }

    @VisibleForTesting
    public Cinema setCinemaId(Long cinemaId) {
        this.cinemaId = cinemaId;
        return this;
    }

    public Cinema setWebsite(String website) {
        this.website = website;
        return this;
    }

    public Long getCinemaId() {
        return cinemaId;
    }
}
