package com.nekodev.paulina.sadowska.nacodokina.movies

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nekodev.paulina.sadowska.nacodokina.R
import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.utils.invisible
import com.nekodev.paulina.sadowska.nacodokina.utils.show
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.partial_movie_list_item.view.*

/**
 * Created by Paulina Sadowska on 30.04.2018.
 */
internal class MovieListAdapter(private val movies: List<Movie>,
                                private val clickListener: OnMovieItemClickListener,
                                private val context: Context) : RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.partial_movie_list_item, parent, false)
        return MovieViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.textTitle.text = movie.title
        holder.itemView.setOnClickListener({ clickListener.onMovieItemClick(movies[position].id) })

        if (movie.posterUrl.isNotBlank()) {
            holder.imagePoster.show()
            Picasso.get()
                    .load(movie.posterUrl)
                    .into(holder.imagePoster)
        } else {
            //todo - show placeholder or something
            holder.imagePoster.invisible()
        }
    }

    interface OnMovieItemClickListener {
        fun onMovieItemClick(movieId: Long)
    }
}


internal class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val imagePoster = view.image_movie_poster!!
    val textTitle = view.movieTitle!!
}