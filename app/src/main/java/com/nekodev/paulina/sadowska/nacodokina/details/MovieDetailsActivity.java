package com.nekodev.paulina.sadowska.nacodokina.details;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.utils.ActivityUtils;
import com.synnapps.carouselview.CarouselView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 18.09.2017.
 */

public class MovieDetailsActivity extends AppCompatActivity {

    private static final String MOVIE_ID_EXTRA = "movie_id_extra_key";
    private static final String CINEMA_IDS_EXTRA = "cinema_ids_extra_key";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.movie_details_media_carousel)
    CarouselView mMediaCarousel;

    @Inject
    MovieDetailsPresenter mPresenter;

    public static void startActivity(Context context, Long id, List<Long> cinemaIds) {
        context.startActivity(getStartIntent(context, id, cinemaIds));
    }

    private static Intent getStartIntent(Context context, Long id, List<Long> cinemaIds) {
        return new Intent(context, MovieDetailsActivity.class)
                .putExtra(MOVIE_ID_EXTRA, id)
                .putExtra(CINEMA_IDS_EXTRA, new ArrayList<>(cinemaIds));
    }

    @VisibleForTesting
    static Intent getStartIntent(Long id, List<Long> cinemaIds) {
        return new Intent()
                .putExtra(MOVIE_ID_EXTRA, id)
                .putExtra(CINEMA_IDS_EXTRA, new ArrayList<>(cinemaIds));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_details_activity);

        // Set up the toolbar.
        ButterKnife.bind(this);
        initializeActionBar();

        MovieDetailsFragment detailsFragment = (MovieDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        Long movieId = getIntent().getLongExtra(MOVIE_ID_EXTRA, -1L);
        List<Long> cinemaIds = (List) getIntent().getSerializableExtra(CINEMA_IDS_EXTRA);
        if (movieId < 0 || cinemaIds == null) {
            finish();
        }

        if (detailsFragment == null) {
            detailsFragment = MovieDetailsFragment.Companion.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    detailsFragment, R.id.contentFrame);
        }

        DaggerMovieDetailsComponent.builder()
                .applicationComponent(((MoviesApplication) getApplication()).getAppComponent())
                .movieDetailsModule(new MovieDetailsModule(detailsFragment, movieId, cinemaIds))
                .build()
                .inject(this);
    }

    @SuppressLint("RestrictedApi")
    private void initializeActionBar() {
        mToolbar.setCollapsible(true);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    CarouselView getMediaCarouselView() {
        return mMediaCarousel;
    }

}
