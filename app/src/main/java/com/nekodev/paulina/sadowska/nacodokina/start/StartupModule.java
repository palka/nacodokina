package com.nekodev.paulina.sadowska.nacodokina.start;

import android.app.Activity;

import com.nekodev.paulina.sadowska.nacodokina.location.GmsLocationProvider;
import com.nekodev.paulina.sadowska.nacodokina.location.LocationProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

@Module
class StartupModule {

    private final StartupContract.View mView;
    private final Activity mActivity;

    StartupModule(Activity mActivity, StartupContract.View view) {
        this.mActivity = mActivity;
        this.mView = view;
    }

    @Provides
    StartupContract.View providesView() {
        return mView;
    }

    @Provides
    LocationProvider providesLocationManager() {
        return new GmsLocationProvider(mActivity);
    }
}
