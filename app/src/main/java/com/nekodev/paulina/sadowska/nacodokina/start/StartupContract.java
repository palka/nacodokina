package com.nekodev.paulina.sadowska.nacodokina.start;

import android.support.annotation.StringRes;

import com.nekodev.paulina.sadowska.nacodokina.base.LegacyBasePresenter;
import com.nekodev.paulina.sadowska.nacodokina.base.BaseView;

import java.util.List;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

interface StartupContract {
    interface View extends BaseView<StartupContract.Presenter> {

        void showLoginPrompt();

        void hideLoginPrompt();

        void showLogOut();

        void openLogin();

        void showLogOutSuccess();

        void showError(@StringRes int errorMessage);

        void startLoading();

        void stopLoading();

        void showMoviesForCinemas(List<Long> cinemaIds);

        void initializeCityPicker(List<String> popularCities);
    }

    interface Presenter extends LegacyBasePresenter {
        void search(String radius, String selectedCity, boolean distanceChecked, boolean cityChecked);

        void signIn();

        void dismissLoginPrompt();

        void signOut();

        void onLoginSuccess();
    }
}
