package com.nekodev.paulina.sadowska.nacodokina.movies

import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Paulina Sadowska on 30.04.2018.
 */

internal class MoviesPresenter @Inject
constructor(private val view: MovieListContract.View,
            private val repository: CinemasRepository,
            private val schedulerProvider: BaseSchedulerProvider,
            private val cinemaIds: List<Long>,
            private val credentialsManager: CredentialsManager) : MovieListContract.Presenter {

    private val disposable: CompositeDisposable = CompositeDisposable()

    @Inject
    fun setupListeners() {
        view.setPresenter(this)
    }

    override fun subscribe() {
        getMovies()
    }

    override fun onItemClicked(movieId: Long) {
        view.showMovieDetails(movieId, cinemaIds)
    }

    private fun getMovies() {
        disposable.add(chooseGetMoviesSingle()
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe({ movies ->
                    view.hideLoading()
                    view.showMovies(movies.filter { it.posterUrl.isNotBlank() })
                }, {
                    view.hideLoading()
                    view.showCriticalError()
                }))
    }

    private fun chooseGetMoviesSingle(): Single<List<Movie>> {
        return if (credentialsManager.areCredentialsSaved()) {
            repository.getMovies(cinemaIds, credentialsManager.token)
        } else {
            repository.getMovies(cinemaIds)
        }
    }

    override fun unSubscribe() {
        disposable.clear()
    }
}
