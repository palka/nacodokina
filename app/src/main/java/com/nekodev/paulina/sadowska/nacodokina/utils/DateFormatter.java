package com.nekodev.paulina.sadowska.nacodokina.utils;

import java.util.Date;

/**
 * Created by Paulina Sadowska on 26.09.2017.
 */

public interface DateFormatter {
    String formatDate(String date);

    String formatTime(String date);

    Date parse(String date);
}
