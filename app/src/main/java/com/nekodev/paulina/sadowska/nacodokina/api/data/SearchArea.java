package com.nekodev.paulina.sadowska.nacodokina.api.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Paulina Sadowska on 24.09.2017.
 */

public class SearchArea implements Parcelable {

    public static final Creator<SearchArea> CREATOR = new Creator<SearchArea>() {
        @Override
        public SearchArea createFromParcel(Parcel source) {
            return new SearchArea(source);
        }

        @Override
        public SearchArea[] newArray(int size) {
            return new SearchArea[size];
        }
    };

    @SerializedName("center")
    private Coordinates center;

    @SerializedName("radiusInKm")
    private double radiusInKm;

    public SearchArea(Coordinates center, double radiusInKm) {
        this.center = center;
        this.radiusInKm = radiusInKm;
    }

    protected SearchArea(Parcel in) {
        this.center = in.readParcelable(Coordinates.class.getClassLoader());
        this.radiusInKm = in.readDouble();
    }

    public double getRadiusInMeters() {
        return getRadiusInKm() * 1000;
    }

    public double getRadiusInKm() {
        return radiusInKm;
    }

    public double getLongitude() {
        return center.getLongitude();
    }

    public double getLatitude() {
        return center.getLatitude();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.center, flags);
        dest.writeDouble(this.radiusInKm);
    }
}
