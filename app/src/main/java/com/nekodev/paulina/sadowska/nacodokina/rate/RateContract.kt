package com.nekodev.paulina.sadowska.nacodokina.rate

import android.support.annotation.StringRes
import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.base.BasePresenter

/**
 * Created by Paulina Sadowska on 24.05.2018.
 */
interface RateContract {

    interface View {
        fun showMovie(currentMovie: Movie)
        fun showMessage(@StringRes messageRes: Int)
        fun enableRateButton(enable: Boolean)
        fun resetRatingBar()
        fun finishActivity()
        fun showLoader()
        fun hideLoader()
    }

    interface Presenter : BasePresenter<View> {
        fun onRateClicked(rating: Int)
        fun onNotSeenClicked()
        fun onRatingChanged(rating: Float)
    }
}