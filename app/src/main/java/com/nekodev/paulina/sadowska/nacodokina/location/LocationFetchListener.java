package com.nekodev.paulina.sadowska.nacodokina.location;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */

public interface LocationFetchListener {
    void onSuccess(double latitude, double longitude);

    void onError();
}
