package com.nekodev.paulina.sadowska.nacodokina.start;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.utils.ActivityUtils;

import java.util.List;

import javax.inject.Inject;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

public class StartupActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final int RC_LOCATION_PERMISSION = 9274;

    @Inject
    StartupPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);
        methodRequiresTwoPermission();

        StartupFragment startupFragment = (StartupFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (startupFragment == null) {
            startupFragment = StartupFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    startupFragment, R.id.contentFrame);
        }

        DaggerStartupComponent.builder()
                .startupModule(new StartupModule(this, startupFragment))
                .applicationComponent(((MoviesApplication) getApplication()).getAppComponent())
                .build()
                .inject(this);
    }

    @AfterPermissionGranted(RC_LOCATION_PERMISSION)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location),
                    RC_LOCATION_PERMISSION, perms);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onPermissionsGranted(int i, List<String> list) {

    }
}
