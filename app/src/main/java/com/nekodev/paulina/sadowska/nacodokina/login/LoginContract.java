package com.nekodev.paulina.sadowska.nacodokina.login;

import android.support.annotation.StringRes;

import com.nekodev.paulina.sadowska.nacodokina.base.LegacyBasePresenter;
import com.nekodev.paulina.sadowska.nacodokina.base.BaseView;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

interface LoginContract {
    interface View extends BaseView<Presenter> {
        void logInSuccess();

        void onError(@StringRes int errorMassage);

        void onLoginFormError(@StringRes int errorMassage);

        void onPasswordFormError(@StringRes int errorMassage);

        void clearFormErrors();

        void startLoading();

        void stopLoading();

        void showRepeatPassword();

        void showSignUpPrompt();

        void setLayoutTitle(@StringRes int title);

        void showRegisterSuccessAndFinish();

        void showRateActivity();
    }

    interface Presenter extends LegacyBasePresenter {
        void signIn(String login, String password, String repeatPassword);
    }
}
