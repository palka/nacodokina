package com.nekodev.paulina.sadowska.nacodokina.viewcomponents;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.nacodokina.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 19.09.2017.
 */

public class PropertyImageView extends LinearLayout {
    @BindView(R.id.property_title)
    TextView mTitle;

    @BindView(R.id.property_value)
    TextView mValue;

    @BindView(R.id.property_icon)
    ImageView mIcon;

    private CharSequence mTitleText;
    private Drawable mIconResource;

    public PropertyImageView(Context context) {
        super(context);
        mTitleText = "";
        initializeViews(context);
    }

    public PropertyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeAttrs(context, attrs);
        initializeViews(context);

    }

    private void initializeAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PropertyImageView);
        mTitleText = typedArray.getText(R.styleable.PropertyImageView_prop_title);
        mIconResource = typedArray.getDrawable(R.styleable.PropertyImageView_prop_icon);
        typedArray.recycle();
    }


    @SuppressLint("SetTextI18n")
    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.property_image_layout, this, true);
        ButterKnife.bind(this, view);
        mTitle.setText(mTitleText + ":");
        if (mIconResource != null) {
            mIcon.setImageDrawable(mIconResource);
        }
    }

    public void setText(String value) {
        mValue.setText(value);
    }

    @VisibleForTesting
    public String getValue() {
        return mValue.getText().toString();
    }
}
