package com.nekodev.paulina.sadowska.nacodokina.api;

import com.nekodev.paulina.sadowska.nacodokina.api.data.Cinema;
import com.nekodev.paulina.sadowska.nacodokina.api.data.Show;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

public interface NaCoDoKinaService {

    @GET("/v1/Movies")
    Flowable<List<Long>> getMovies(@Query("Center.Latitude") double latitude, @Query("Center.Longitude") double longitude, @Query("Radius") double radius);

    @GET("/v1/Movies/{id}")
    Flowable<Movie> getMovie(@Path("id") Long id);

    @GET("/v1/Movies/{id}/cinemas")
    Flowable<List<Cinema>> getCinemas(@Path("id") Long id, @Query("Center.Latitude") double latitude, @Query("Center.Longitude") double longitude, @Query("Radius") double radius);

    @GET("/v1/Movies/{id}/cinemas/{cinemaId}/showtimes")
    Flowable<List<Show>> getShows(@Path("id") Long id, @Path("cinemaId") Long cinemaId);

    @POST("/v1/Movies/{id}/rating")
    Flowable<Object> saveRating(@Path("id") Long id, @Body double rating);
}

