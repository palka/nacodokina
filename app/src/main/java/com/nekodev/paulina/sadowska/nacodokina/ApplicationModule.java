package com.nekodev.paulina.sadowska.nacodokina;

import android.content.Context;
import android.content.SharedPreferences;

import com.nekodev.paulina.sadowska.nacodokina.credentials.ConcealEncryption;
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.credentials.Encryption;
import com.nekodev.paulina.sadowska.nacodokina.credentials.SharedPrefsCredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.utils.ApiDateFormatter;
import com.nekodev.paulina.sadowska.nacodokina.utils.DateFormatter;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

@Module
class ApplicationModule {

    private final Context mContext;

    ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }

    @Provides
    BaseSchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }

    @Provides
    CredentialsManager provideCredentialsManager(SharedPreferences sharedPreferences, Encryption encryption) {
        return new SharedPrefsCredentialsManager(sharedPreferences, encryption);
    }

    @Provides
    SharedPreferences providesSharedPreferences() {
        return mContext.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
    }

    @Provides
    Encryption providesEncryption() {
        return new ConcealEncryption(mContext);
    }

    @Provides
    DateFormatter providesDateFormatter() {
        return new ApiDateFormatter();
    }
}
