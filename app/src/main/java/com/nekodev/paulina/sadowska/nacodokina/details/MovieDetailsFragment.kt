package com.nekodev.paulina.sadowska.nacodokina.details

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import com.nekodev.paulina.sadowska.nacodokina.R
import com.nekodev.paulina.sadowska.nacodokina.api.CinemaWithShowtimesResponse
import com.nekodev.paulina.sadowska.nacodokina.api.MovieDetailsResponse
import com.nekodev.paulina.sadowska.nacodokina.api.data.MovieRating
import com.nekodev.paulina.sadowska.nacodokina.base.BaseFragment
import com.nekodev.paulina.sadowska.nacodokina.utils.YoutubeUtil
import com.nekodev.paulina.sadowska.nacodokina.utils.hide
import com.nekodev.paulina.sadowska.nacodokina.utils.show
import com.nekodev.paulina.sadowska.nacodokina.utils.timestampToDate
import com.nekodev.paulina.sadowska.nacodokina.viewcomponents.MediaLayout
import kotlinx.android.synthetic.main.movie_details_fragment.*
import org.jetbrains.anko.support.v4.toast


/**
 * Created by Paulina Sadowska on 18.09.2017.
 */

class MovieDetailsFragment : BaseFragment<MovieDetailsContract.Presenter>(), MovieDetailsContract.View {

    companion object {
        private const val RELEASE_DATE_FORMAT = "d MMMM yyyy"

        fun newInstance(): MovieDetailsFragment {
            return MovieDetailsFragment()
        }
    }

    private var cinemasAdapter: CinemasAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.movie_details_fragment, container, false)
    }

    override fun showDetails(movieDetails: MovieDetailsResponse) {
        textTitle.text = movieDetails.title
        textOriginalTitle.text = movieDetails.originalTitle
        propertyCrew.setText(movieDetails.crew.joinToString())
        propertyGenre.setText(movieDetails.genres.joinToString())
        propertyDuration.setText(getMovieLengthText(movieDetails.length))
        textDescription.text = movieDetails.description
        propertyAgeLimit.setText(movieDetails.ageLimit)
        propertyDirector.setText(movieDetails.director)
        propertyReleaseDate.setText(getFormattedReleaseDate(movieDetails.releaseDateTimestamp))
    }

    override fun showRatingBar() {
        ratingBar.show()
        rateButton.show()
        propertyRatingTitle.show()

        ratingBar.setOnRatingChangeListener { _, _ -> presenter.onRatingChanged() }
        rateButton.setOnClickListener { presenter.onRateClicked(ratingBar.rating) }
    }

    override fun initializeRatingBar(rating: Double?) {
        ratingBar.rating = rating?.toFloat() ?: 0f
    }

    private fun getFormattedReleaseDate(releaseDateTimestamp: Long): String {
        return if (releaseDateTimestamp > 0) {
            releaseDateTimestamp.timestampToDate(RELEASE_DATE_FORMAT)
        } else {
            "brak informacji"
        }
    }

    private fun getMovieLengthText(length: Int): String {
        return resources.getQuantityString(R.plurals.movie_length, length, length)
    }

    override fun initializeDaysPicker(dates: List<String>) {
        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, dates)
        spinnerDatePicker.adapter = adapter
        spinnerDatePicker.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                cinemasAdapter?.setCurrentDate(parent?.getItemAtPosition(position).toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                //do nothing
            }
        }
    }

    override fun showCinemaShows(cinemaWithShows: CinemaWithShowtimesResponse, currentDate: String) {
        if (cinemasAdapter == null) {
            initializeCinemasAdapter(currentDate)
        }

        cinemasAdapter?.add(cinemaWithShows)
    }

    private fun initializeCinemasAdapter(currentDate: String) {
        with(recyclerViewShows) {
            setHasFixedSize(true)
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            cinemasAdapter = CinemasAdapter(currentDate)
            adapter = cinemasAdapter
        }
    }

    override fun showRatings(ratings: List<MovieRating>) {
        textReviewsTitle.show()
        with(recyclerViewReviews) {
            setHasFixedSize(true)
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = ReviewsAdapter(ratings)
        }
    }

    override fun showPhotosCarousel(media: List<MediaResource>) {
        with((activity as MovieDetailsActivity).mediaCarouselView) {
            setViewListener { position ->
                val mediaResource = media[position]
                val isVideo = mediaResource.isPlayButtonVisible()
                MediaLayout(context!!)
                        .setImageUrl(mediaResource.getImageUrl(), isVideo)
                        .setPlayButtonVisibility(isVideo)
            }
            pageCount = media.size
            setImageClickListener { position -> presenter.onMediaClicked(media[position]) }
        }
    }

    override fun showError() {
        toast(R.string.error_occurred)
    }

    override fun setRating(rating: Int) {
        if (rating > 0) {
            ratingBar.rating = rating.toFloat()
            propertyRatingTitle.setText(R.string.your_rating)
        }
    }

    override fun enableRateButton() {
        rateButton.isEnabled = true
    }

    override fun startRatingLoading() {
        progressBarRating.show()
        rateButton.hide()
    }

    override fun stopRatingLoading() {
        progressBarRating.hide()
        rateButton.show()
    }

    override fun playVideo(videoId: String) {
        YoutubeUtil.play(activity, videoId)
    }

    override fun ratingSaved() {
        toast(R.string.rating_saved)
    }
}
