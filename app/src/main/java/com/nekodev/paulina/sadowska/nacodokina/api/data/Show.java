package com.nekodev.paulina.sadowska.nacodokina.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Paulina Sadowska on 28.09.2017.
 */

/*
{
    "language": "string",
    "showType": "string",
    "cinemaName": "string",
    "cinemaId": 0,
    "movieId": 0,
    "movieName": "string",
    "showTime": "2017-09-28T09:37:28.070Z"
  }
 */

public class Show {

    @SerializedName("movieId")
    private Long movieId;

    @SerializedName("cinemaId")
    private Long cinemaId;

    @SerializedName("language")
    private String language;

    @SerializedName("showType")
    private String type;

    @SerializedName("showTime")
    private String time;

    public Show(String type, String time) {
        this.time = time;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getTime() {
        return time;
    }
}
