package com.nekodev.paulina.sadowska.nacodokina.details;

import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 18.09.2017.
 */
@Module
class MovieDetailsModule {
    private final MovieDetailsContract.View view;
    private final Long movieId;
    private final List<Long> cinemaIds;

    MovieDetailsModule(MovieDetailsContract.View view, Long movieId, List<Long> cinemaIds) {
        this.view = view;
        this.movieId = movieId;
        this.cinemaIds = cinemaIds;
    }

    @Provides
    MovieDetailsContract.View providesView() {
        return view;
    }

    @Provides
    Long providesMovieId() {
        return movieId;
    }

    @Provides
    List<Long> providesCinemaIds() {
        return cinemaIds;
    }

}

