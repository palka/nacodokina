package com.nekodev.paulina.sadowska.nacodokina.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Paulina Sadowska on 24.09.2017.
 */

public class AuthData {

    @SerializedName("userName")
    private String login;

    @SerializedName("password")
    private String password;

    public AuthData(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
