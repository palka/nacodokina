package com.nekodev.paulina.sadowska.nacodokina.movies;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;

import com.nekodev.paulina.sadowska.nacodokina.MoviesApplication;
import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.utils.ActivityUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Paulina Sadowska on 16.09.2017.
 */

public class MoviesActivity extends AppCompatActivity {

    private static final String EXTRA_CINEMA_IDS = "extra_cinema_ids";

    @Inject
    MoviesPresenter mPresenter;


    public static void startActivity(Context context, List<Long> cinemaIds) {
        context.startActivity(getStartIntent(context, cinemaIds));
    }

    public static Intent getStartIntent(Context context, List<Long> cinemaIds) {
        return new Intent(context, MoviesActivity.class)
                .putExtra(EXTRA_CINEMA_IDS, new ArrayList<>(cinemaIds));
    }

    @VisibleForTesting
    public static Intent getStartIntent(List<Long> cinemaIds) {
        return new Intent()
                .putExtra(EXTRA_CINEMA_IDS, new ArrayList<>(cinemaIds));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);

        MovieListFragment movieListFragment = (MovieListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);
        if (movieListFragment == null) {
            movieListFragment = MovieListFragment.Factory.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    movieListFragment, R.id.contentFrame);
        }
        Bundle extras = getIntent().getExtras();
        DaggerMoviesComponent.builder()
                .applicationComponent(((MoviesApplication) getApplication()).getAppComponent())
                .moviesModule(new MoviesModule(movieListFragment, (List) extras.getSerializable(EXTRA_CINEMA_IDS)))
                .build()
                .inject(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
