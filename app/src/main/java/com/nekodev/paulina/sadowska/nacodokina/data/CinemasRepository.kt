package com.nekodev.paulina.sadowska.nacodokina.data

import com.nekodev.paulina.sadowska.nacodokina.api.*
import com.nekodev.paulina.sadowska.nacodokina.api.data.MovieRating
import com.nekodev.paulina.sadowska.nacodokina.api.data.SearchArea
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

/**
 * Created by Paulina Sadowska on 29.04.2018.
 */
class CinemasRepository @Inject constructor(private val service: CinemasService) {

    fun getCinemasInArea(searchArea: SearchArea): Single<List<Long>> {
        return service.getCinemasByLocation(latitude = searchArea.latitude,
                longitude = searchArea.longitude,
                radiusInKm = searchArea.radiusInKm)
    }

    fun getCinemasInCity(cityName: String): Single<List<Long>> {
        return service.getCinemasInCity(cityName)
    }

    fun getMovies(cinemasIds: List<Long>, authToken: String? = null): Single<List<Movie>> {
        return service.getMoviesInCinemas(cinemaIds = cinemasIds, authToken = authToken)
    }

    fun getMovieDetails(movieId: Long): Single<MovieDetailsResponse> {
        return service.getMovieDetails(movieId)
    }

    fun getRatings(movieId: Long): Single<List<MovieRating>> {
        return service.getMovieRatings(movieId)
                .map { ratingsResponse -> toRatingList(ratingsResponse) }
    }

    fun getCinemaShows(movieId: Long, cinemas: List<Long>): Flowable<CinemaWithShowtimesResponse> {
        if (cinemas.isEmpty()) {
            return Flowable.empty()
        }

        return Flowable.fromIterable(cinemas).flatMap {
            service.getShowtimesOfMovie(movieId, it, System.currentTimeMillis() / 1_000)
                    .toFlowable()
                    .onErrorResumeNext(Flowable.empty())
                    .filter { !it.showsInDay.keys.isEmpty() }
        }
    }

    private fun toRatingList(ratingsResponse: RatingResponse): List<MovieRating> {
        val reviews = ArrayList<MovieRating>()
        with(ratingsResponse) {
            reviews.add(MovieRating("Filmweb", filmwebRating))
            reviews.add(MovieRating("IMDB", imdbRating))
            reviews.add(MovieRating("Metacritic", metacriticRating))
            reviews.add(MovieRating("Rotten Tomatoes", rottenTomatoesRating))
        }
        return reviews.filter { it.rating > 0 }
    }

    fun getMoviePhotos(movieId: Long): Single<List<String>> {
        return service.getMoviePhotos(movieId)
    }

    fun getMovieTrailer(movieId: Long): Single<TrailerResponse> {
        return service.getMovieTrailer(movieId)
    }

    fun rate(movieId: Long, rating: Double, authToken: String): Completable {
        return service.rate(movieId, authToken, rating)
    }

    fun getUserRating(movieId: Long, authToken: String): Single<Double> {
        return service.getUserRating(movieId, authToken)
    }

    fun fetchPopularMoviesToRate(): Single<List<Movie>> {
        return  service.getPopularMovies()
    }

    fun login(login: String, password: String): Single<AuthTokenResponse> {
        return service.login(login, password)
    }

    fun register(login: String, password: String): Single<AuthTokenResponse> {
        return service.register(login, password)
    }
}