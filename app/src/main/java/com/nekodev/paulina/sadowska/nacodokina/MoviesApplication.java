package com.nekodev.paulina.sadowska.nacodokina;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

public class MoviesApplication extends Application {

    private ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

    }

    public ApplicationComponent getAppComponent() {
        return mComponent;
    }

    @VisibleForTesting
    public void setAppComponent(ApplicationComponent component) {
        this.mComponent = component;
    }
}
