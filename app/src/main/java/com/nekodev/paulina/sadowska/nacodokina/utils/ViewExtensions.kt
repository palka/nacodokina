package com.nekodev.paulina.sadowska.nacodokina.utils

import android.view.View

/**
 * Created by Paulina Sadowska on 30.04.2018.
 */
fun View.show(show: Boolean) {
    if (show) {
        show()
    } else {
        hide()
    }
}

fun View.setInvisibleOrShow(invisible: Boolean) {
    if (invisible) {
        invisible()
    } else {
        show()
    }
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}