package com.nekodev.paulina.sadowska.nacodokina.movies

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.ButterKnife
import com.nekodev.paulina.sadowska.nacodokina.R
import com.nekodev.paulina.sadowska.nacodokina.api.Movie
import com.nekodev.paulina.sadowska.nacodokina.base.BaseFragment
import com.nekodev.paulina.sadowska.nacodokina.details.MovieDetailsActivity
import com.nekodev.paulina.sadowska.nacodokina.utils.hide
import com.nekodev.paulina.sadowska.nacodokina.utils.show
import kotlinx.android.synthetic.main.movie_list_fragment.*

/**
 * Created by Paulina Sadowska on 30.04.2018.
 */
internal class MovieListFragment : MovieListContract.View,
        BaseFragment<MovieListContract.Presenter>(),
        MovieListAdapter.OnMovieItemClickListener {

    companion object Factory {
        fun newInstance(): MovieListFragment = MovieListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.movie_list_fragment, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun showMovies(movies: List<Movie>) {
        recyclerViewMovieList.layoutManager = GridLayoutManager(context, 2)
        recyclerViewMovieList.itemAnimator = DefaultItemAnimator()
        recyclerViewMovieList.adapter = MovieListAdapter(movies = movies,
                context = context!!,
                clickListener = this)
    }

    override fun onMovieItemClick(movieId: Long) {
        presenter.onItemClicked(movieId)
    }

    override fun showMovieDetails(movieId: Long, cinemaIds: List<Long>) {
        MovieDetailsActivity.startActivity(context, movieId, cinemaIds)
    }

    override fun hideLoading() {
        textMovieListProgressLabel.hide()
        ProgressMovieList.hide()
        recyclerViewMovieList.show()
    }


    override fun showCriticalError() {
        recyclerViewMovieList.hide()
        textMovieListError.show()
    }

    override fun onError(errorMessage: Int) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }
}