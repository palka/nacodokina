package com.nekodev.paulina.sadowska.nacodokina.viewcomponents;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.nacodokina.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Paulina Sadowska on 28.09.2017.
 */

class TimeView extends FrameLayout {

    @BindView(R.id.show_time)
    TextView mTime;

    public TimeView(Context context) {
        super(context);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.show_time_layout, this, true);
        ButterKnife.bind(this, view);
    }

    public void setText(String text) {
        mTime.setText(text);
    }

    public void setTextWithLink(String time, String bookingPath) {
        if (TextUtils.isEmpty(bookingPath)) {
            mTime.setText(time);
            mTime.setClickable(false);
        } else {
            mTime.setText(Html.fromHtml("<a href=\"" + bookingPath + "\">" + time + "</a>"));
            mTime.setClickable(true);
            mTime.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }
}
