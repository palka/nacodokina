package com.nekodev.paulina.sadowska.nacodokina.start;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.base.BaseFragment;
import com.nekodev.paulina.sadowska.nacodokina.login.LoginActivity;
import com.nekodev.paulina.sadowska.nacodokina.movies.MoviesActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

public class StartupFragment extends BaseFragment<StartupContract.Presenter> implements StartupContract.View {

    private static final int REQUEST_CODE_USER_LOGGED_IN = 765;

    @BindView(R.id.startup_login_layout)
    RelativeLayout mLoginLayout;

    @BindView(R.id.startup_log_out_button)
    Button mLogoutButton;

    @BindView(R.id.startup_radius_edit)
    EditText mRadiusEdit;

    @BindView(R.id.startup_progress)
    ProgressBar mProgress;

    @BindView(R.id.spinnerCityPicker)
    Spinner spinner;

    @BindView(R.id.startup_radio_button_distance)
    RadioButton distanceRadioButton;

    @BindView(R.id.startup_radio_button_city)
    RadioButton cityRadioButton;

    private ArrayAdapter<String> citiesAdapter;

    public static StartupFragment newInstance() {
        return new StartupFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.startup_fragment, container, false);
        ButterKnife.bind(this, view);
        distanceRadioButton.setChecked(true);
        distanceRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> cityRadioButton.setChecked(!isChecked));
        cityRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> distanceRadioButton.setChecked(!isChecked));
        return view;
    }

    @OnClick(R.id.startup_search_button)
    void onSearchClicked() {
        String selectedCity = citiesAdapter.getItem(spinner.getSelectedItemPosition());
        String distance = mRadiusEdit.getText().toString();
        presenter.search(distance, selectedCity, distanceRadioButton.isChecked(), cityRadioButton.isChecked());
    }

    @OnClick(R.id.startup_login_button)
    void onSignInClicked() {
        presenter.signIn();
    }

    @OnClick(R.id.startup_dismiss_button)
    void onDismissClicked() {
        presenter.dismissLoginPrompt();
    }

    @OnClick(R.id.startup_log_out_button)
    void onSignOutClicked() {
        presenter.signOut();
    }

    @Override
    public void showLoginPrompt() {
        mLoginLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginPrompt() {
        mLoginLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLogOut() {
        mLogoutButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void openLogin() {
        startActivityForResult(LoginActivity.getStartIntent(getContext()), REQUEST_CODE_USER_LOGGED_IN);
    }

    @Override
    public void showLogOutSuccess() {
        Toast.makeText(getContext(), R.string.log_out_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(int errorMessage) {
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startLoading() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void showMoviesForCinemas(List<Long> cinemaIds) {
        MoviesActivity.startActivity(getContext(), cinemaIds);
    }

    @Override
    public void initializeCityPicker(List<String> cities) {
        citiesAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, cities);
        spinner.setAdapter(citiesAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_USER_LOGGED_IN) {
            if (resultCode == RESULT_OK) {
                presenter.onLoginSuccess();
            }
        }
    }
}
