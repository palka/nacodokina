package com.nekodev.paulina.sadowska.nacodokina.api

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Paulina Sadowska on 16.01.2018.
 */
@Parcelize
data class Movie(val id: Long,
                 val title: String,
                 val posterUrl: String) : Parcelable

data class MovieDetailsResponse(val id: Long,
                                val title: String,
                                val originalTitle: String,
                                val description: String,
                                val length: Int,
                                val ageLimit: String,
                                val releaseDateTimestamp: Long,
                                val director: String,
                                val crew: List<String>,
                                val genres: List<String>,
                                val posterUrl: String,
                                val backdropUrl: String?)

data class TrailerResponse(val movieId: Long,
                           val youtubeId: String,
                           val title: String,
                           val thumbnail: String)

data class RatingResponse(val movieId: Long,
                          val filmwebRating: Double,
                          val imdbRating: Double,
                          val metacriticRating: Double,
                          val rottenTomatoesRating: Double)

data class CinemaWithShowtimesResponse(val cinema: CinemaResponse,
                                       val showsInDay: Map<String, List<ShowtimesInDay>>)

data class ShowtimesInDay(val types: List<String>,
                          val shows: List<ShowWithLinkResponse>)

data class ShowWithLinkResponse(val movieId: Long,
                                val timestamp: Long,
                                val bookingPath: String)

data class CinemaResponse(var name: String,
                          var address: String,
                          var city: String,
                          var latitude: Double,
                          var longitude: Double,
                          var email: String,
                          var phone: String)

data class GenreResponse(var id: Long,
                         var name: String)

data class PersonResponse(var id: Long,
                          var name: String,
                          var photoUrl: String)

data class AuthTokenResponse(var token: String,
                             var ratedMoviesCount: Int)