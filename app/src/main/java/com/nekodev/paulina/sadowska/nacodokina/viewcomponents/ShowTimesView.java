package com.nekodev.paulina.sadowska.nacodokina.viewcomponents;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.api.ShowWithLinkResponse;
import com.nekodev.paulina.sadowska.nacodokina.utils.DateFormatExtensionsKt;

import org.apmem.tools.layouts.FlowLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 28.09.2017.
 */

public class ShowTimesView extends LinearLayout {

    @BindView(R.id.show_type)
    TextView mType;

    @BindView(R.id.show_times_layout)
    FlowLayout mTimes;

    public ShowTimesView(Context context) {
        super(context);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.show_times_layout, this, true);
        ButterKnife.bind(this, view);
    }

    public ShowTimesView setType(List<String> showType) {
        mType.setText(TextUtils.join(", ", showType));
        return this;
    }


    public void addShow(ShowWithLinkResponse show) {
        TimeView timeView = new TimeView(mTimes.getContext());
        String time = DateFormatExtensionsKt.timestampToDate(show.getTimestamp() * 1000, "HH:mm");
        timeView.setTextWithLink(time, show.getBookingPath());
        mTimes.addView(timeView);
    }
}
