package com.nekodev.paulina.sadowska.nacodokina.start;

import com.nekodev.paulina.sadowska.nacodokina.ApplicationComponent;
import com.nekodev.paulina.sadowska.nacodokina.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = StartupModule.class)
public interface StartupComponent {
    void inject(StartupActivity startupActivity);
}
