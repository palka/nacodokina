package com.nekodev.paulina.sadowska.nacodokina.rate

import com.nekodev.paulina.sadowska.nacodokina.ApplicationComponent
import com.nekodev.paulina.sadowska.nacodokina.scopes.FragmentScope
import dagger.Component

/**
 * Created by Paulina Sadowska on 26.05.2018.
 */
@FragmentScope
@Component(dependencies = [(ApplicationComponent::class)])
interface RateComponent {
    fun inject(activity: RateActivity)
}