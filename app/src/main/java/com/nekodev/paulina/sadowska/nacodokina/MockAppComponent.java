package com.nekodev.paulina.sadowska.nacodokina;

import android.accounts.NetworkErrorException;

import com.nekodev.paulina.sadowska.nacodokina.api.AuthTokenResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.CinemaResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.CinemaWithShowtimesResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.CinemasService;
import com.nekodev.paulina.sadowska.nacodokina.api.GenreResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.Movie;
import com.nekodev.paulina.sadowska.nacodokina.api.MovieDetailsResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.PersonResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.RatingResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.TrailerResponse;
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.ImmediateSchedulerProvider;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by Paulina Sadowska on 27.09.2017.
 */

public class MockAppComponent implements ApplicationComponent {

    public static final String ORIGINAL_TITLE = "ORIGINAL_TITLE";
    public static final String TITLE = "TITLE";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String CREW = "crew";
    public static final String GENRE = "genre";
    public static final String AGE_LIMIT = "AGE_LIMIT";
    public static final String DIRECTOR = "director";
    public static final String DURATION = "120 minut";

    public static final String CORRECT_LOGIN = "login";
    public static final String CORRECT_PASSWORD = "password";

    @Override
    public CredentialsManager getCredentialsManager() {
        return new CredentialsManager() {
            @Override
            public void saveCredentials(String token) {
            }

            @Override
            public String getToken() {
                return "token";
            }

            @Override
            public boolean areCredentialsSaved() {
                return true;
            }

            @Override
            public void resetCredentials() {

            }
        };
    }

    @Override
    public CinemasRepository getCinemasRepository() {
        return new CinemasRepository(new CinemasService() {
            @NotNull
            @Override
            public Single<Map<String, List<Long>>> getAllCinemasInCities() {
                return null;
            }

            @NotNull
            @Override
            public Single<List<Long>> getCinemasByLocation(double latitude, double longitude, double radiusInKm) {
                return Single.just(Collections.emptyList());
            }

            @NotNull
            @Override
            public Single<List<Long>> getCinemasInCity(@NotNull String cityName) {
                return Single.just(Collections.emptyList());
            }

            @NotNull
            @Override
            public Single<CinemaResponse> getCinemaDetails(long cinemaId) {
                return null;
            }

            @NotNull
            @Override
            public Single<List<Movie>> getMoviesInCinemas(@NotNull List<Long> cinemaIds, @Nullable String authToken) {
                return Single.just(Collections.emptyList());
            }

            @NotNull
            @Override
            public Single<List<Movie>> getPopularMovies() {
                return Single.error(new Throwable());
            }

            @NotNull
            @Override
            public Single<CinemaWithShowtimesResponse> getShowtimesOfMovie(long movieId, long cinemaId, long timestampAfter, @Nullable Long timestampBefore, @Nullable Integer limit) {
                return Single.error(NetworkErrorException::new);
            }

            @NotNull
            @Override
            public Single<MovieDetailsResponse> getMovieDetails(long movieId) {
                return Single.just(new MovieDetailsResponse(
                        0L,
                        TITLE,
                        ORIGINAL_TITLE,
                        DESCRIPTION,
                        120,
                        AGE_LIMIT,
                        0L,
                        DIRECTOR,
                        Collections.singletonList(CREW),
                        Collections.singletonList(GENRE),
                        "",
                        ""
                ));
            }

            @NotNull
            @Override
            public Single<List<GenreResponse>> getAllGenres() {
                return Single.just(Collections.emptyList());
            }

            @NotNull
            @Override
            public Single<List<GenreResponse>> getGenresOfMovie(long movieId) {
                return Single.just(Collections.emptyList());
            }

            @NotNull
            @Override
            public Single<List<String>> getMoviePhotos(long movieId) {
                return Single.just(Collections.emptyList());
            }

            @NotNull
            @Override
            public Single<TrailerResponse> getMovieTrailer(long movieId) {
                return Single.error(NetworkErrorException::new);
            }

            @NotNull
            @Override
            public Single<RatingResponse> getMovieRatings(long movieId) {
                RatingResponse ratings = new RatingResponse(1, 1.1, 2.2, 33, 44);
                return Single.just(ratings);
            }

            @NotNull
            @Override
            public Completable rate(long movieId, @NotNull String userToken, double rating) {
                return Completable.complete();
            }

            @NotNull
            @Override
            public Single<Double> getUserRating(long movieId, @NotNull String userToken) {
                return Single.just(4.4);
            }

            @NotNull
            @Override
            public Single<PersonResponse> getPerson(long personId) {
                return null;
            }

            @NotNull
            @Override
            public Single<AuthTokenResponse> login(@NotNull String login, @NotNull String password) {
                if (login.equals(CORRECT_LOGIN) && password.equals(CORRECT_PASSWORD)) {
                    return Single.just(new AuthTokenResponse("token", 3));
                } else {
                    return Single.error(Throwable::new);
                }
            }

            @NotNull
            @Override
            public Single<AuthTokenResponse> register(@NotNull String login, @NotNull String password) {
                if (login.equals(CORRECT_LOGIN) && password.equals(CORRECT_PASSWORD)) {
                    return Single.just(new AuthTokenResponse("token", 0));
                } else {
                    return Single.error(Throwable::new);
                }
            }
        });
    }

    @Override
    public BaseSchedulerProvider getSchedulerProvider() {
        return new ImmediateSchedulerProvider();
    }
}
