package com.nekodev.paulina.sadowska.nacodokina.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Paulina Sadowska on 06.05.2018.
 */

@SuppressLint("SimpleDateFormat")
fun Long.timestampToDate(dateFormat: String) : String {
    return Date(this).toString(dateFormat)
}

@SuppressLint("SimpleDateFormat")
fun Date.toString(dateFormat: String) : String {
    return SimpleDateFormat(dateFormat).format(this)
}