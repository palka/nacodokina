package com.nekodev.paulina.sadowska.nacodokina.details;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.api.CinemaResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.CinemaWithShowtimesResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.ShowWithLinkResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.ShowtimesInDay;
import com.nekodev.paulina.sadowska.nacodokina.utils.ViewExtensionsKt;
import com.nekodev.paulina.sadowska.nacodokina.viewcomponents.ShowTimesView;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paulina Sadowska on 20.09.2017.
 */

class CinemasAdapter extends RecyclerView.Adapter<CinemasAdapter.CinemaViewHolder> {

    private final List<CinemaWithShowtimesResponse> cinemasWithShowtimes = new ArrayList<>();
    private String currentDate;

    CinemasAdapter(String currentDate) {
        this.currentDate = currentDate;
    }

    @Override
    public CinemaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cinema, parent, false);
        return new CinemaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CinemaViewHolder holder, int position) {
        CinemaWithShowtimesResponse cinemaWithShowtimes = cinemasWithShowtimes.get(position);
        List<ShowtimesInDay> showsInDay = cinemaWithShowtimes
                .getShowsInDay()
                .get(currentDate);

        holder.bind(cinemaWithShowtimes.getCinema(), showsInDay);
    }

    public void setCurrentDate(String currentDate) {
        if (!this.currentDate.equals(currentDate)) {
            this.currentDate = currentDate;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return cinemasWithShowtimes.size();
    }

    public void add(CinemaWithShowtimesResponse cinemaWithShowtimes) {
        cinemasWithShowtimes.add(cinemaWithShowtimes);
        notifyItemChanged(getItemCount() - 2);
    }

    class CinemaViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cinema_name)
        TextView mName;
        @BindView(R.id.cinema_address)
        TextView mAddress;
        @BindView(R.id.cinema_shows)
        LinearLayout mShows;

        CinemaViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(CinemaResponse cinema, @Nullable List<ShowtimesInDay> showtimes) {
            mShows.removeAllViews();

            if (showtimes != null) {
                showAllViews(true, mName, mAddress, mShows);
                applyData(cinema, showtimes);
            } else {
                showAllViews(false, mName, mAddress, mShows);
            }
        }

        private void applyData(CinemaResponse cinema, @NonNull List<ShowtimesInDay> showtimes) {
            mName.setText(cinema.getName());
            mAddress.setText(cinema.getAddress());

            for (ShowtimesInDay showtime : showtimes) {
                ShowTimesView timesView = new ShowTimesView(mShows.getContext()).setType(showtime.getTypes());
                for (ShowWithLinkResponse show : showtime.getShows()) {
                    timesView.addShow(show);
                }

                mShows.addView(timesView);
            }
        }

        private void showAllViews(boolean show, View... views) {
            for (View view : views) {
                ViewExtensionsKt.show(view, show);
            }
        }
    }
}
