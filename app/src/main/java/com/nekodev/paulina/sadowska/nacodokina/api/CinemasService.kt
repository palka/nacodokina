package com.nekodev.paulina.sadowska.nacodokina.api

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Paulina Sadowska on 29.04.2018.
 */
interface CinemasService {

    @GET("/cinemasInCities")
    fun getAllCinemasInCities(): Single<Map<String, List<Long>>>

    @GET("/cinemas")
    fun getCinemasByLocation(@Query("latitude") latitude: Double,
                             @Query("longitude") longitude: Double,
                             @Query("radiusInKm") radiusInKm: Double): Single<List<Long>>

    @GET("/cinemas/{cityName}")
    fun getCinemasInCity(@Path("cityName") cityName: String): Single<List<Long>>

    @GET("/cinemas/{cinemaId}/details")
    fun getCinemaDetails(@Path("cinemaId") cinemaId: Long): Single<CinemaResponse>

    @GET("/movies")
    fun getMoviesInCinemas(@Query("cinema") cinemaIds: List<Long>,
                           @Query("token") authToken: String? = null): Single<List<Movie>>

    @GET("/movies/popular")
    fun getPopularMovies(): Single<List<Movie>>

    @GET("/movies/{movieId}/showtimes")
    fun getShowtimesOfMovie(@Path("movieId") movieId: Long,
                            @Query("cinema") cinemaId: Long,
                            @Query("timestampAfter") timestampAfter: Long,
                            @Query("timestampBefore") timestampBefore: Long? = null,
                            @Query("limit") limit: Int? = null): Single<CinemaWithShowtimesResponse>

    @GET("/movies/{movieId}")
    fun getMovieDetails(@Path("movieId") movieId: Long): Single<MovieDetailsResponse>

    @GET("/genres/popular")
    fun getAllGenres(): Single<List<GenreResponse>>

    @GET("/movies/{movieId}/genres")
    fun getGenresOfMovie(@Path("movieId") movieId: Long): Single<List<GenreResponse>>

    @GET("/movies/{movieId}/photos")
    fun getMoviePhotos(@Path("movieId") movieId: Long): Single<List<String>>

    @GET("/movies/{movieId}/trailer")
    fun getMovieTrailer(@Path("movieId") movieId: Long): Single<TrailerResponse>

    @GET("/movies/{movieId}/ratings")
    fun getMovieRatings(@Path("movieId") movieId: Long): Single<RatingResponse>

    @POST("/movies/{movieId}//user/rating")
    fun rate(@Path("movieId") movieId: Long,
             @Query("token") userToken: String,
             @Query("rating") rating: Double): Completable

    @GET("/movies/{movieId}//user/rating")
    fun getUserRating(@Path("movieId") movieId: Long,
                      @Query("token") userToken: String): Single<Double>

    @GET("/people/{personId}")
    fun getPerson(@Path("personId") personId: Long): Single<PersonResponse>

    @POST("/login")
    fun login(@Query("login") login: String,
              @Query("password") password: String): Single<AuthTokenResponse>

    @POST("/register")
    fun register(@Query("login") login: String,
                 @Query("password") password: String): Single<AuthTokenResponse>
}
