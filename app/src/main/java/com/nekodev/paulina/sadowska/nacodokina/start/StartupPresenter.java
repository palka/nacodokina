package com.nekodev.paulina.sadowska.nacodokina.start;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.nekodev.paulina.sadowska.nacodokina.R;
import com.nekodev.paulina.sadowska.nacodokina.api.data.Coordinates;
import com.nekodev.paulina.sadowska.nacodokina.api.data.SearchArea;
import com.nekodev.paulina.sadowska.nacodokina.credentials.CredentialsManager;
import com.nekodev.paulina.sadowska.nacodokina.data.CinemasRepository;
import com.nekodev.paulina.sadowska.nacodokina.location.LocationFetchListener;
import com.nekodev.paulina.sadowska.nacodokina.location.LocationProvider;
import com.nekodev.paulina.sadowska.nacodokina.utils.schedulers.BaseSchedulerProvider;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

public class StartupPresenter implements StartupContract.Presenter {

    private static final int MIN_RADIUS = 1;
    private static final int MAX_RADIUS = 1000;
    private static final List<String> POPULAR_CITIES = Arrays.asList("Bydgoszcz", "Gdańsk", "Gdynia", "Katowice", "Kielce",
            "Kraków", "Łódź", "Poznań", "Szczecin", "Warszawa", "Wrocław");

    private final StartupContract.View view;
    private final LocationProvider locationProvider;
    private final CredentialsManager credentialsManager;
    private final CinemasRepository repository;
    private final BaseSchedulerProvider schedulerProvider;
    private final CompositeDisposable disposable;

    private Coordinates coordinates;

    @Inject
    StartupPresenter(@NonNull StartupContract.View view,
                     @NotNull LocationProvider locationProvider,
                     @NotNull CredentialsManager credentialsManager,
                     @NotNull CinemasRepository repository,
                     @NotNull BaseSchedulerProvider schedulerProvider) {
        this.view = view;
        this.locationProvider = locationProvider;
        this.credentialsManager = credentialsManager;
        this.repository = repository;
        this.schedulerProvider = schedulerProvider;
        disposable = new CompositeDisposable();
    }

    @Inject
    void setupListeners() {
        view.setPresenter(this);
    }

    @Override
    public void subscribe() {
        locationProvider.fetchLocation(new LocationFetchListener() {
            @Override
            public void onSuccess(double latitude, double longitude) {
                coordinates = new Coordinates(latitude, longitude);
            }

            @Override
            public void onError() {
            }
        });
        view.initializeCityPicker(POPULAR_CITIES);
        if (credentialsManager.areCredentialsSaved()) {
            view.showLogOut();
        } else {
            view.showLoginPrompt();
        }
    }

    @Override
    public void search(String radius, String selectedCity, boolean distanceChecked, boolean cityChecked) {
        view.startLoading();
        if (distanceChecked) {
            searchByDistance(radius);
        } else if (cityChecked) {
            fetchCinemasInCity(selectedCity);
        }
    }

    private void searchByDistance(String radius) {
        if (TextUtils.isEmpty(radius)) {
            view.showError(R.string.error_incorrect_radius);
            return;
        }

        int radiusParsed;
        try {
            radiusParsed = Integer.parseInt(radius);
        } catch (NumberFormatException e) {
            view.showError(R.string.error_incorrect_radius);
            return;
        }

        if (radiusParsed < MIN_RADIUS || radiusParsed > MAX_RADIUS) {
            view.showError(R.string.error_incorrect_radius);
            return;
        }

        if (coordinates == null) {
            fetchLocation(radiusParsed);
        } else {
            fetchCinemas(coordinates, radiusParsed);
        }
    }

    private void fetchLocation(int radiusParsed) {
        locationProvider.fetchLocation(new LocationFetchListener() {
            @Override
            public void onSuccess(double latitude, double longitude) {
                coordinates = new Coordinates(latitude, longitude);
                fetchCinemas(coordinates, radiusParsed);
            }

            @Override
            public void onError() {
                view.showError(R.string.location_error);
                view.stopLoading();
            }
        });
    }

    private void fetchCinemas(Coordinates coordinates, int radiusParsed) {
        SearchArea searchArea = new SearchArea(coordinates, radiusParsed);
        disposable.add(repository.getCinemasInArea(searchArea)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doFinally(view::stopLoading)
                .subscribe(view::showMoviesForCinemas,
                        error -> view.showError(R.string.error_occurred)));
    }


    private void fetchCinemasInCity(String selectedCity) {
        disposable.add(repository.getCinemasInCity(selectedCity)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doFinally(view::stopLoading)
                .subscribe(view::showMoviesForCinemas,
                        error -> view.showError(R.string.error_occurred)));
    }

    @Override
    public void signIn() {
        view.openLogin();
    }

    @Override
    public void dismissLoginPrompt() {
        view.hideLoginPrompt();
    }

    @Override
    public void signOut() {
        credentialsManager.resetCredentials();
        view.showLogOutSuccess();
        view.showLoginPrompt();
    }

    @Override
    public void onLoginSuccess() {
        view.hideLoginPrompt();
        view.showLogOut();
    }

    @Override
    public void unSubscribe() {
    }
}
