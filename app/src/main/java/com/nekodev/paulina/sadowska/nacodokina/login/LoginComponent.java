package com.nekodev.paulina.sadowska.nacodokina.login;

import com.nekodev.paulina.sadowska.nacodokina.ApplicationComponent;
import com.nekodev.paulina.sadowska.nacodokina.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by Paulina Sadowska on 22.09.2017.
 */

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
