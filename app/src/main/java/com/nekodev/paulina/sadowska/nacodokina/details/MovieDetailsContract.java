package com.nekodev.paulina.sadowska.nacodokina.details;

import com.nekodev.paulina.sadowska.nacodokina.api.CinemaWithShowtimesResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.MovieDetailsResponse;
import com.nekodev.paulina.sadowska.nacodokina.api.data.MovieRating;
import com.nekodev.paulina.sadowska.nacodokina.base.LegacyBasePresenter;
import com.nekodev.paulina.sadowska.nacodokina.base.BaseView;

import java.util.List;

/**
 * Created by Paulina Sadowska on 18.09.2017.
 */

public class MovieDetailsContract {
    public interface View extends BaseView<MovieDetailsContract.Presenter> {

        void showDetails(MovieDetailsResponse movieDetails);

        void showCinemaShows(CinemaWithShowtimesResponse cinemaWithShows, String currentDate);

        void showRatings(List<MovieRating> reviews);

        void showPhotosCarousel(List<MediaResource> media);

        void ratingSaved();

        void showError();

        void setRating(int rating);

        void enableRateButton();

        void startRatingLoading();

        void stopRatingLoading();

        void playVideo(String videoId);

        void showRatingBar();

        void initializeRatingBar(Double rating);

        void initializeDaysPicker(List<String> dates);
    }

    public interface Presenter extends LegacyBasePresenter {

        void onRateClicked(float rating);

        void onRatingChanged();

        void onMediaClicked(MediaResource mediaResource);
    }
}
