package com.nekodev.paulina.sadowska.nacodokina.movies;

import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paulina Sadowska on 17.09.2017.
 */

@Module
class MoviesModule {

    private final MovieListContract.View mView;
    private final List<Long> cinemaIds;

    MoviesModule(MovieListContract.View view, List<Long> cinemaIds) {
        this.mView = view;
        this.cinemaIds = cinemaIds;
    }

    @Provides
    MovieListContract.View providesView() {
        return mView;
    }

    @Provides
    List<Long> providesCinemaIds() {
        return cinemaIds;
    }

}
